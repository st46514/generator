package com.o2.generatorCDR;

import com.o2.generatorCDR.controllers.StageController;
import com.o2.generatorCDR.configuration.AppConfig;
import com.o2.generatorCDR.controllers.SpringController;
import javafx.application.Application;
import javafx.stage.Stage;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main extends Application{



    @Override
    public void start(Stage primaryStage){
        SpringController springController = new SpringController();
        ApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
        springController.setApplicationContext(context);
        StageController stageController = context.getBean(StageController.class);
        stageController.init(primaryStage);
        stageController.setCdrM2MScene();

    }


    public static void main(String[] args) {
        launch(args);
    }




    //   cdrGenerator.generateAllXML("420792007455","230029500027614","roaming");
      //  cdrGenerator.generateAllVoice("420792007455","230029500027614", "pmo_test125");
      //  cdrGenerator.generateAllData("420792007455","230029500027614","pmo_test126");
      //  cdrGenerator.generateAllSMS("420792007455","230029500027614","pmo_test127");

     //   cdrGenerator.generateSpecificXML("420792007455","230029500027614","roamingSpecific","SWEEP","CZEET","420720123123");
     //   cdrGenerator.generateSpecificData("420792007455","230029500027614","160.218.032.068","dataSpecific",2048,"07");


}
