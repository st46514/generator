package com.o2.generatorCDR.controllers;

import com.o2.generatorCDR.entity.*;
import com.o2.generatorCDR.service.Invoice.CSVReader;
import com.o2.generatorCDR.service.Invoice.DetailCreator;
import com.o2.generatorCDR.service.Invoice.InvoiceCreator;
import com.o2.generatorCDR.service.Invoice.SummaryCreator;
import com.o2.generatorCDR.stages.HashDialog;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.FlowPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.springframework.context.ApplicationContext;
import sun.misc.BASE64Encoder;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.File;
import java.net.URL;
import java.nio.file.Paths;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.*;

public class InvoiceController implements Initializable{

    private CSVReader csvReader;
    private InvoiceCreator invoiceCreator;
    private SummaryCreator summaryCreator;
    private DetailCreator detailCreator;
    private StageController stageController;
    private ApplicationContext context;
    private Tab payer;
    private Tab summary;
    private Tab detail;
@FXML
AnchorPane invoiceAnchorPane;


    @FXML
    TabPane tabPanel;

@FXML
MenuItem createHashButton;



    @FXML
    public void handleSwitchScene() {
        stageController.setCdrM2MScene();
    }

    @FXML
    public void handleCreateHashButton(){
        HashDialog hashDialog = new HashDialog(invoiceAnchorPane.getScene().getWindow());
        hashDialog.showAndWait();
    }


    @FXML
    public void handleFileChooserMenu() {
        FileChooser fileChooser = new FileChooser();


        String currentPath = Paths.get(".").toAbsolutePath().normalize().toString();
        fileChooser.setInitialDirectory(new File(currentPath));

        File file = fileChooser.showOpenDialog(new Stage());

        if (!invoiceCreator.getInvoiceHeaderList().isEmpty())
            invoiceCreator.getInvoiceHeaderList().removeAll(invoiceCreator.getInvoiceHeaderList());
        if (!invoiceCreator.getInvoiceDataList().isEmpty())
            invoiceCreator.getInvoiceDataList().removeAll(invoiceCreator.getInvoiceDataList());
        if (!invoiceCreator.getInvoiceTaxList().isEmpty())
            invoiceCreator.getInvoiceTaxList().removeAll(invoiceCreator.getInvoiceTaxList());

        if (!summaryCreator.getSummaryHeaderList().isEmpty())
            summaryCreator.getSummaryHeaderList().removeAll(summaryCreator.getSummaryHeaderList());
        if (!summaryCreator.getSummaryFreeUnitsList().isEmpty())
            summaryCreator.getSummaryFreeUnitsList().removeAll(summaryCreator.getSummaryFreeUnitsList());
        if (!summaryCreator.getSummaryRecurrentFeeList().isEmpty())
            summaryCreator.getSummaryRecurrentFeeList().removeAll(summaryCreator.getSummaryRecurrentFeeList());
        if (!summaryCreator.getSummaryUsageList().isEmpty())
            summaryCreator.getSummaryUsageList().removeAll(summaryCreator.getSummaryUsageList());
        if (!summaryCreator.getSummaryPartyServicesList().isEmpty())
            summaryCreator.getSummaryPartyServicesList().removeAll(summaryCreator.getSummaryPartyServicesList());
        if (!summaryCreator.getSummaryCreditList().isEmpty())
            summaryCreator.getSummaryCreditList().removeAll(summaryCreator.getSummaryCreditList());

        if (!detailCreator.getDetailHeaderList().isEmpty())
            detailCreator.getDetailHeaderList().removeAll(detailCreator.getDetailHeaderList());
        if (!detailCreator.getDetailDataList().isEmpty())
            detailCreator.getDetailDataList().removeAll(detailCreator.getDetailDataList());

        csvReader.readCSV(file);


        ObservableList<InvoiceHeader> invoiceHeaderObservableList = FXCollections.observableArrayList(invoiceCreator.getInvoiceHeaderList());
        ObservableList<InvoiceData> invoiceDataObservableList = FXCollections.observableArrayList(invoiceCreator.getInvoiceDataList());
        ObservableList<InvoiceTax> invoiceTaxObservableList = FXCollections.observableArrayList(invoiceCreator.getInvoiceTaxList());

        ObservableList<SummaryHeader> summaryHeaderObservableList = FXCollections.observableArrayList(summaryCreator.getSummaryHeaderList());
        ObservableList<SummaryFreeUnits> summaryFreeUnitsObservableList = FXCollections.observableArrayList(summaryCreator.getSummaryFreeUnitsList());
        ObservableList<SummaryRecurrentFee> summaryRecurrentFeeObservableList = FXCollections.observableArrayList(summaryCreator.getSummaryRecurrentFeeList());
        ObservableList<SummaryUsage> summaryUsageObservableList = FXCollections.observableArrayList(summaryCreator.getSummaryUsageList());
        ObservableList<SummaryPartyServices> summaryPartyServicesObservableList = FXCollections.observableArrayList(summaryCreator.getSummaryPartyServicesList());
        ObservableList<SummaryCredit> summaryCreditObservableList = FXCollections.observableArrayList(summaryCreator.getSummaryCreditList());

        ObservableList<DetailHeader> detailHeaderObservableList = FXCollections.observableArrayList(detailCreator.getDetailHeaderList());
        ObservableList<DetailData> detailDataObservableList = FXCollections.observableArrayList(detailCreator.getDetailDataList());




        payer.setContent(createInvoiceTabPane(invoiceHeaderObservableList,invoiceDataObservableList,invoiceTaxObservableList));
        summary.setContent(createSummaryTabPane(summaryHeaderObservableList,summaryFreeUnitsObservableList,summaryRecurrentFeeObservableList,summaryUsageObservableList,
                summaryPartyServicesObservableList,summaryCreditObservableList));
        detail.setContent(createDetailTabPane(detailHeaderObservableList,detailDataObservableList));




        tabPanel.getTabs().add(payer);
        tabPanel.getTabs().add(summary);
        tabPanel.getTabs().add(detail);





    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        context = SpringController.getApplicationContext();
        stageController = context.getBean(StageController.class);
        csvReader = context.getBean(CSVReader.class);
        invoiceCreator = context.getBean(InvoiceCreator.class);
        summaryCreator = context.getBean(SummaryCreator.class);
        detailCreator = context.getBean(DetailCreator.class);
        payer = new Tab("Payer");
        summary = new Tab("Summary");
        detail = new Tab("Detail");




    }



    private ObservableList<TableColumn<InvoiceHeader,String>> createInvoiceHeaderColumns(){
        ObservableList<TableColumn<InvoiceHeader,String>> tableColumns = FXCollections.observableArrayList();
        TableColumn<InvoiceHeader,String> payerReferenceColumn = new TableColumn<>("PayerReference");
        payerReferenceColumn.setMinWidth(100);
        payerReferenceColumn.setCellValueFactory(new PropertyValueFactory<>("payerReference"));

        TableColumn<InvoiceHeader,String> billingPeriodStartColumn = new TableColumn<>("BillingPeriodStart");
        billingPeriodStartColumn.setMinWidth(100);
        billingPeriodStartColumn.setCellValueFactory(new PropertyValueFactory<>("billingPeriodStart"));

        TableColumn<InvoiceHeader,String> billingPeriodEndColumn = new TableColumn<>("BillingPeriodEnd");
        billingPeriodEndColumn.setMinWidth(100);
        billingPeriodEndColumn.setCellValueFactory(new PropertyValueFactory<>("billingPeriodEnd"));

        TableColumn<InvoiceHeader,String> extPayerIdColumn = new TableColumn<>("ExtPayerId");
        extPayerIdColumn.setMinWidth(100);
        extPayerIdColumn.setCellValueFactory(new PropertyValueFactory<>("extPayerId"));

        TableColumn<InvoiceHeader,String> finalPriceIncludingVATColumn = new TableColumn<>("FinalPriceIncludingVAT");
        finalPriceIncludingVATColumn.setMinWidth(100);
        finalPriceIncludingVATColumn.setCellValueFactory(new PropertyValueFactory<>("finalPriceIncludingVAT"));

        tableColumns.addAll(payerReferenceColumn,billingPeriodStartColumn,billingPeriodEndColumn,extPayerIdColumn,finalPriceIncludingVATColumn);

        return tableColumns;
    }
    private ObservableList<TableColumn<InvoiceData,String>> createInvoiceDataColumns(){
        ObservableList<TableColumn<InvoiceData,String>> tableColumns = FXCollections.observableArrayList();

        TableColumn<InvoiceData,String> payerReferenceColumn = new TableColumn<>("PayerReference");
        payerReferenceColumn.setMinWidth(100);
        payerReferenceColumn.setCellValueFactory(new PropertyValueFactory<>("payerReference"));

        TableColumn<InvoiceData,String> serviceCategoryColumn = new TableColumn<>("ServiceCategory");
        serviceCategoryColumn.setMinWidth(100);
        serviceCategoryColumn.setCellValueFactory(new PropertyValueFactory<>("serviceCategory"));

        TableColumn<InvoiceData,String> priceWithoutVATColumn = new TableColumn<>("PriceWithoutVAT");
        priceWithoutVATColumn.setMinWidth(100);
        priceWithoutVATColumn.setCellValueFactory(new PropertyValueFactory<>("priceWithoutVAT"));

        TableColumn<InvoiceData,String> VATPriceColumn = new TableColumn<>("VATPrice");
        VATPriceColumn.setMinWidth(100);
        VATPriceColumn.setCellValueFactory(new PropertyValueFactory<>("VATPrice"));

        TableColumn<InvoiceData,String> priceIncludingVATColumn = new TableColumn<>("priceIncludingVAT");
        priceIncludingVATColumn.setMinWidth(100);
        priceIncludingVATColumn.setCellValueFactory(new PropertyValueFactory<>("priceIncludingVAT"));

        tableColumns.addAll(payerReferenceColumn,serviceCategoryColumn,priceWithoutVATColumn,VATPriceColumn,priceIncludingVATColumn);

        return tableColumns;
    }
    private ObservableList<TableColumn<InvoiceTax,String>> createInvoiceTaxColumns(){
        ObservableList<TableColumn<InvoiceTax,String>> tableColumns = FXCollections.observableArrayList();

        TableColumn<InvoiceTax,String> payerReferenceColumn = new TableColumn<>("PayerReference");
        payerReferenceColumn.setMinWidth(100);
        payerReferenceColumn.setCellValueFactory(new PropertyValueFactory<>("payerReference"));

        TableColumn<InvoiceTax,String> priceWithoutVATColumn = new TableColumn<>("PriceWithoutVAT");
        priceWithoutVATColumn.setMinWidth(100);
        priceWithoutVATColumn.setCellValueFactory(new PropertyValueFactory<>("priceWithoutVAT"));

        TableColumn<InvoiceTax,String> VATPriceColumn = new TableColumn<>("VATPrice");
        VATPriceColumn.setMinWidth(100);
        VATPriceColumn.setCellValueFactory(new PropertyValueFactory<>("VATPrice"));

        TableColumn<InvoiceTax,String> priceIncludingVATColumn = new TableColumn<>("priceIncludingVAT");
        priceIncludingVATColumn.setMinWidth(100);
        priceIncludingVATColumn.setCellValueFactory(new PropertyValueFactory<>("priceIncludingVAT"));

        tableColumns.addAll(payerReferenceColumn,priceWithoutVATColumn,VATPriceColumn,priceIncludingVATColumn);

        return tableColumns;
    }

    private ObservableList<TableColumn<SummaryHeader,String>> createSummaryHeaderColumns(){
        ObservableList<TableColumn<SummaryHeader,String>> observableList = FXCollections.observableArrayList();

        TableColumn<SummaryHeader,String> subscriptionId = new TableColumn<>("SubscriptionId");
        subscriptionId.setMinWidth(100);
        subscriptionId.setCellValueFactory(new PropertyValueFactory<>("subscriptionId"));

        TableColumn<SummaryHeader,String> msisdn = new TableColumn<>("Msisdn");
        msisdn.setMinWidth(100);
        msisdn.setCellValueFactory(new PropertyValueFactory<>("msisdn"));

        TableColumn<SummaryHeader,String> currencyCode = new TableColumn<>("CurrencyCode");
        currencyCode.setMinWidth(100);
        currencyCode.setCellValueFactory(new PropertyValueFactory<>("currencyCode"));

        TableColumn<SummaryHeader,String> billingPeriodStart = new TableColumn<>("BillingPeriodStart");
        billingPeriodStart.setMinWidth(100);
        billingPeriodStart.setCellValueFactory(new PropertyValueFactory<>("billingPeriodStart"));

        TableColumn<SummaryHeader,String> billingPeriodEnd = new TableColumn<>("BillingPeriodEnd");
        billingPeriodEnd.setMinWidth(100);
        billingPeriodEnd.setCellValueFactory(new PropertyValueFactory<>("billingPeriodEnd"));

        TableColumn<SummaryHeader,String> tariffName = new TableColumn<>("TariffName");
        tariffName.setMinWidth(100);
        tariffName.setCellValueFactory(new PropertyValueFactory<>("tariffName"));

        observableList.addAll(subscriptionId,msisdn,currencyCode,billingPeriodStart,billingPeriodEnd,tariffName);

        return observableList;
    }
    private ObservableList<TableColumn<SummaryFreeUnits,String>> createSummaryFreeUnitsColumns(){
        ObservableList<TableColumn<SummaryFreeUnits,String>> observableList = FXCollections.observableArrayList();

        TableColumn<SummaryFreeUnits,String> subscriptionId = new TableColumn<>("SubscriptionId");
        subscriptionId.setMinWidth(100);
        subscriptionId.setCellValueFactory(new PropertyValueFactory<>("subscriptionId"));

        TableColumn<SummaryFreeUnits,String> unitType = new TableColumn<>("UnitType");
        unitType.setMinWidth(100);
        unitType.setCellValueFactory(new PropertyValueFactory<>("unitType"));

        TableColumn<SummaryFreeUnits,String> InitialCounterValue = new TableColumn<>("InitialCounterValue");
        InitialCounterValue.setMinWidth(100);
        InitialCounterValue.setCellValueFactory(new PropertyValueFactory<>("InitialCounterValue"));

        TableColumn<SummaryFreeUnits,String> used = new TableColumn<>("Used");
        used.setMinWidth(100);
        used.setCellValueFactory(new PropertyValueFactory<>("used"));

        TableColumn<SummaryFreeUnits,String> transferred = new TableColumn<>("Transferred");
        transferred.setMinWidth(100);
        transferred.setCellValueFactory(new PropertyValueFactory<>("transferred"));

        TableColumn<SummaryFreeUnits,String> rollover = new TableColumn<>("Rollover");
        rollover.setMinWidth(100);
        rollover.setCellValueFactory(new PropertyValueFactory<>("rollover"));

        observableList.addAll(subscriptionId,unitType,InitialCounterValue,used,transferred,rollover);

        return observableList;
    }
    private ObservableList<TableColumn<SummaryRecurrentFee,String>> createSummaryRecurrentFeeColumns(){
        ObservableList<TableColumn<SummaryRecurrentFee,String>> observableList = FXCollections.observableArrayList();

        TableColumn<SummaryRecurrentFee,String> subscriptionId = new TableColumn<>("SubscriptionId");
        subscriptionId.setMinWidth(100);
        subscriptionId.setCellValueFactory(new PropertyValueFactory<>("subscriptionId"));

        TableColumn<SummaryRecurrentFee,String> serviceID = new TableColumn<>("ServiceID");
        serviceID.setMinWidth(100);
        serviceID.setCellValueFactory(new PropertyValueFactory<>("serviceID"));

        TableColumn<SummaryRecurrentFee,String> serviceName = new TableColumn<>("serviceName");
        serviceName.setMinWidth(100);
        serviceName.setCellValueFactory(new PropertyValueFactory<>("serviceName"));

        TableColumn<SummaryRecurrentFee,String> chargingCode = new TableColumn<>("ChargingCode");
        chargingCode.setMinWidth(100);
        chargingCode.setCellValueFactory(new PropertyValueFactory<>("chargingCode"));

        TableColumn<SummaryRecurrentFee,String> startDate = new TableColumn<>("StartDate");
        startDate.setMinWidth(100);
        startDate.setCellValueFactory(new PropertyValueFactory<>("startDate"));

        TableColumn<SummaryRecurrentFee,String> endDate = new TableColumn<>("EndDate");
        endDate.setMinWidth(100);
        endDate.setCellValueFactory(new PropertyValueFactory<>("endDate"));

        TableColumn<SummaryRecurrentFee,String> amount = new TableColumn<>("Amount");
        amount.setMinWidth(100);
        amount.setCellValueFactory(new PropertyValueFactory<>("amount"));

        TableColumn<SummaryRecurrentFee,String> sumPriceBefore = new TableColumn<>("SumPriceBefore");
        sumPriceBefore.setMinWidth(100);
        sumPriceBefore.setCellValueFactory(new PropertyValueFactory<>("sumPriceBefore"));

        TableColumn<SummaryRecurrentFee,String> sumPriceAfter = new TableColumn<>("sumPriceAfter");
        sumPriceAfter.setMinWidth(100);
        sumPriceAfter.setCellValueFactory(new PropertyValueFactory<>("sumPriceAfter"));

        TableColumn<SummaryRecurrentFee,String> sumWholeSalePrice = new TableColumn<>("sumWholeSalePrice");
        sumWholeSalePrice.setMinWidth(100);
        sumWholeSalePrice.setCellValueFactory(new PropertyValueFactory<>("sumWholeSalePrice"));

        observableList.addAll(subscriptionId,serviceID,serviceName,chargingCode,startDate,endDate,amount,sumPriceBefore,sumPriceAfter,sumWholeSalePrice);

        return observableList;
    }
    private ObservableList<TableColumn<SummaryUsage,String>> createSummaryUsageColumns(){
        ObservableList<TableColumn<SummaryUsage,String>> observableList = FXCollections.observableArrayList();

        TableColumn<SummaryUsage,String> subscriptionId = new TableColumn<>("SubscriptionId");
        subscriptionId.setMinWidth(100);
        subscriptionId.setCellValueFactory(new PropertyValueFactory<>("subscriptionId"));

        TableColumn<SummaryUsage,String> serviceCategory = new TableColumn<>("serviceCategory");
        serviceCategory.setMinWidth(100);
        serviceCategory.setCellValueFactory(new PropertyValueFactory<>("serviceCategory"));

        TableColumn<SummaryUsage,String> serviceCategoryName = new TableColumn<>("serviceCategoryName");
        serviceCategoryName.setMinWidth(100);
        serviceCategoryName.setCellValueFactory(new PropertyValueFactory<>("serviceCategoryName"));

        TableColumn<SummaryUsage,String> chargingCode = new TableColumn<>("chargingCode");
        chargingCode.setMinWidth(100);
        chargingCode.setCellValueFactory(new PropertyValueFactory<>("chargingCode"));

        TableColumn<SummaryUsage,String> serviceID = new TableColumn<>("serviceID");
        serviceID.setMinWidth(100);
        serviceID.setCellValueFactory(new PropertyValueFactory<>("serviceID"));

        TableColumn<SummaryUsage,String> SumDuration = new TableColumn<>("SumDuration");
        SumDuration.setMinWidth(100);
        SumDuration.setCellValueFactory(new PropertyValueFactory<>("SumDuration"));

        TableColumn<SummaryUsage,String> sumEvents = new TableColumn<>("sumEvents");
        sumEvents.setMinWidth(100);
        sumEvents.setCellValueFactory(new PropertyValueFactory<>("sumEvents"));

        TableColumn<SummaryUsage,String> sumVolumeDownloaded = new TableColumn<>("sumVolumeDownloaded");
        sumVolumeDownloaded.setMinWidth(100);
        sumVolumeDownloaded.setCellValueFactory(new PropertyValueFactory<>("sumVolumeDownloaded"));

        TableColumn<SummaryUsage,String> sumVolumeUploaded = new TableColumn<>("sumVolumeUploaded");
        sumVolumeUploaded.setMinWidth(100);
        sumVolumeUploaded.setCellValueFactory(new PropertyValueFactory<>("sumVolumeUploaded"));

        TableColumn<SummaryUsage,String> sumUnits = new TableColumn<>("sumUnits");
        sumUnits.setMinWidth(100);
        sumUnits.setCellValueFactory(new PropertyValueFactory<>("sumUnits"));

        TableColumn<SummaryUsage,String> sumPriceBefore = new TableColumn<>("sumPriceBefore");
        sumPriceBefore.setMinWidth(100);
        sumPriceBefore.setCellValueFactory(new PropertyValueFactory<>("sumPriceBefore"));

        observableList.addAll(subscriptionId,serviceCategory,serviceCategoryName,chargingCode,serviceID,SumDuration,sumEvents,sumVolumeDownloaded,sumVolumeUploaded,sumUnits,sumPriceBefore);


        return observableList;
    }
    private ObservableList<TableColumn<SummaryPartyServices,String>> createSummaryPartyServicesColumns(){
        ObservableList<TableColumn<SummaryPartyServices,String>> observableList = FXCollections.observableArrayList();

        TableColumn<SummaryPartyServices,String> subscriptionId = new TableColumn<>("SubscriptionId");
        subscriptionId.setMinWidth(100);
        subscriptionId.setCellValueFactory(new PropertyValueFactory<>("subscriptionId"));

        TableColumn<SummaryPartyServices,String> serviceCategory = new TableColumn<>("serviceCategory");
        serviceCategory.setMinWidth(100);
        serviceCategory.setCellValueFactory(new PropertyValueFactory<>("serviceCategory"));

        TableColumn<SummaryPartyServices,String> serviceCategoryName = new TableColumn<>("serviceCategoryName");
        serviceCategoryName.setMinWidth(100);
        serviceCategoryName.setCellValueFactory(new PropertyValueFactory<>("serviceCategoryName"));

        TableColumn<SummaryPartyServices,String> chargingCode = new TableColumn<>("chargingCode");
        chargingCode.setMinWidth(100);
        chargingCode.setCellValueFactory(new PropertyValueFactory<>("chargingCode"));

        TableColumn<SummaryPartyServices,String> sumUnits = new TableColumn<>("sumUnits");
        sumUnits.setMinWidth(100);
        sumUnits.setCellValueFactory(new PropertyValueFactory<>("sumUnits"));

        TableColumn<SummaryPartyServices,String> sumFreeUnits = new TableColumn<>("sumFreeUnits");
        sumFreeUnits.setMinWidth(100);
        sumFreeUnits.setCellValueFactory(new PropertyValueFactory<>("sumFreeUnits"));

        TableColumn<SummaryPartyServices,String> sumPriceAfter = new TableColumn<>("sumPriceAfter");
        sumPriceAfter.setMinWidth(100);
        sumPriceAfter.setCellValueFactory(new PropertyValueFactory<>("sumPriceAfter"));

        TableColumn<SummaryPartyServices,String> sumWholeSalePrice = new TableColumn<>("sumWholeSalePrice");
        sumWholeSalePrice.setMinWidth(100);
        sumWholeSalePrice.setCellValueFactory(new PropertyValueFactory<>("sumWholeSalePrice"));

        observableList.addAll(subscriptionId,serviceCategory, serviceCategoryName, chargingCode, sumUnits,sumFreeUnits,sumPriceAfter,sumWholeSalePrice);
        return observableList;
    }
    private ObservableList<TableColumn<SummaryCredit,String>> createSummaryCreditColumns(){
        ObservableList<TableColumn<SummaryCredit,String>> observableList = FXCollections.observableArrayList();

        TableColumn<SummaryCredit,String> subscriptionId = new TableColumn<>("SubscriptionId");
        subscriptionId.setMinWidth(100);
        subscriptionId.setCellValueFactory(new PropertyValueFactory<>("subscriptionId"));

        TableColumn<SummaryCredit,String> itemType = new TableColumn<>("itemType");
        itemType.setMinWidth(100);
        itemType.setCellValueFactory(new PropertyValueFactory<>("itemType"));

        TableColumn<SummaryCredit,String> chargingCode = new TableColumn<>("chargingCode");
        chargingCode.setMinWidth(100);
        chargingCode.setCellValueFactory(new PropertyValueFactory<>("chargingCode"));

        TableColumn<SummaryCredit,String> serviceName = new TableColumn<>("serviceName");
        serviceName.setMinWidth(100);
        serviceName.setCellValueFactory(new PropertyValueFactory<>("serviceName"));

        TableColumn<SummaryCredit,String> amount = new TableColumn<>("amount");
        amount.setMinWidth(100);
        amount.setCellValueFactory(new PropertyValueFactory<>("amount"));

        TableColumn<SummaryCredit,String> sumPriceAfter = new TableColumn<>("sumPriceAfter");
        sumPriceAfter.setMinWidth(100);
        sumPriceAfter.setCellValueFactory(new PropertyValueFactory<>("sumPriceAfter"));

        TableColumn<SummaryCredit,String> sumFinalPrice = new TableColumn<>("sumFinalPrice");
        sumFinalPrice.setMinWidth(100);
        sumFinalPrice.setCellValueFactory(new PropertyValueFactory<>("sumFinalPrice"));

        observableList.addAll(subscriptionId,itemType,chargingCode,serviceName,amount,sumPriceAfter,sumFinalPrice);
        return observableList;
    }

    private ObservableList<TableColumn<DetailHeader, String>> createDetailHeaderColumns(){
        ObservableList<TableColumn<DetailHeader,String>> observableList = FXCollections.observableArrayList();

        TableColumn<DetailHeader,String> subscriptionId = new TableColumn<>("SubscriptionId");
        subscriptionId.setMinWidth(100);
        subscriptionId.setCellValueFactory(new PropertyValueFactory<>("subscriptionId"));

        TableColumn<DetailHeader, String> msisdn = new TableColumn<>("msisdn");
        msisdn.setMinWidth(100);
        msisdn.setCellValueFactory(new PropertyValueFactory<>("msisdn"));

        TableColumn<DetailHeader, String> extSubscriptionId = new TableColumn<>("extSubscriptionId");
        extSubscriptionId.setMinWidth(100);
        extSubscriptionId.setCellValueFactory(new PropertyValueFactory<>("extSubscriptionId"));

        TableColumn<DetailHeader, String> currencyCode = new TableColumn<>("currencyCode");
        currencyCode.setMinWidth(100);
        currencyCode.setCellValueFactory(new PropertyValueFactory<>("currencyCode"));

        observableList.addAll(subscriptionId, msisdn,extSubscriptionId,currencyCode);

        return observableList;
    }

    private ObservableList<TableColumn<DetailData, String>> createDetailDataColumns(){
        ObservableList<TableColumn<DetailData,String>> observableList = FXCollections.observableArrayList();

        TableColumn<DetailData,String> subscriptionId = new TableColumn<>("SubscriptionId");
        subscriptionId.setMinWidth(100);
        subscriptionId.setCellValueFactory(new PropertyValueFactory<>("subscriptionId"));

        TableColumn<DetailData,String> serviceCategory = new TableColumn<>("serviceCategory");
        serviceCategory.setMinWidth(100);
        serviceCategory.setCellValueFactory(new PropertyValueFactory<>("serviceCategory"));

        TableColumn<DetailData,String> chargingCode = new TableColumn<>("chargingCode");
        chargingCode.setMinWidth(100);
        chargingCode.setCellValueFactory(new PropertyValueFactory<>("chargingCode"));

        TableColumn<DetailData,String> serviceID = new TableColumn<>("serviceID");
        serviceID.setMinWidth(100);
        serviceID.setCellValueFactory(new PropertyValueFactory<>("serviceID"));

        TableColumn<DetailData,String> priceBefore = new TableColumn<>("priceBefore");
        priceBefore.setMinWidth(100);
        priceBefore.setCellValueFactory(new PropertyValueFactory<>("priceBefore"));

        TableColumn<DetailData,String> priceAfter = new TableColumn<>("priceAfter");
        priceAfter.setMinWidth(100);
        priceAfter.setCellValueFactory(new PropertyValueFactory<>("priceAfter"));


        observableList.addAll(subscriptionId,serviceCategory,chargingCode,serviceID,priceBefore,priceAfter);
        return observableList;
    }



    private TabPane createInvoiceTabPane(ObservableList<InvoiceHeader> invoiceHeaderObservableList,ObservableList<InvoiceData> invoiceDataObservableList,ObservableList<InvoiceTax> invoiceTaxObservableList){
        TabPane payerTabPane = new TabPane();
        Tab header = new Tab("Header");
        TableView<InvoiceHeader> headerTableView = new TableView<>();
        headerTableView.setItems(invoiceHeaderObservableList);
        headerTableView.getColumns().addAll(createInvoiceHeaderColumns());
        header.setContent(headerTableView);
        payerTabPane.getTabs().add(header);

        Tab data = new Tab("Data");
        TableView<InvoiceData> dataTableView = new TableView<>();
        dataTableView.setItems(invoiceDataObservableList);
        dataTableView.getColumns().addAll(createInvoiceDataColumns());
        data.setContent(dataTableView);
        payerTabPane.getTabs().add(data);


        Tab tax = new Tab("Tax");
        TableView<InvoiceTax> taxTableView = new TableView<>();
        taxTableView.setItems(invoiceTaxObservableList);
        taxTableView.getColumns().addAll(createInvoiceTaxColumns());
        tax.setContent(taxTableView);
        payerTabPane.getTabs().add(tax);
        return payerTabPane;
    }

    private TabPane createSummaryTabPane(ObservableList<SummaryHeader> summaryHeaderObservableList,ObservableList<SummaryFreeUnits> summaryFreeUnitsObservableList,ObservableList<SummaryRecurrentFee> summaryRecurrentFeeObservableList,
                                         ObservableList<SummaryUsage> summaryUsageObservableList,ObservableList<SummaryPartyServices> summaryPartyServicesObservableList,ObservableList<SummaryCredit> summaryCreditObservableList){

        TabPane summaryTabPane = new TabPane();
        Tab header = new Tab("Header");
        TableView<SummaryHeader> headerTableView = new TableView<>();
        headerTableView.setItems(summaryHeaderObservableList);
        headerTableView.getColumns().addAll(createSummaryHeaderColumns());
        header.setContent(headerTableView);
        summaryTabPane.getTabs().add(header);


        Tab freeUnits = new Tab("Free Units");
        TableView<SummaryFreeUnits> summaryFreeUnitsTableView = new TableView<>();
        summaryFreeUnitsTableView.setItems(summaryFreeUnitsObservableList);
        summaryFreeUnitsTableView.getColumns().addAll(createSummaryFreeUnitsColumns());
        freeUnits.setContent(summaryFreeUnitsTableView);
        summaryTabPane.getTabs().add(freeUnits);

        Tab recurrentFee = new Tab("Recurrent Fee");
        TableView<SummaryRecurrentFee> summaryRecurrentFeeTableView = new TableView<>();
        summaryRecurrentFeeTableView.setItems(summaryRecurrentFeeObservableList);
        summaryRecurrentFeeTableView.getColumns().addAll(createSummaryRecurrentFeeColumns());
        recurrentFee.setContent(summaryRecurrentFeeTableView);
        summaryTabPane.getTabs().add(recurrentFee);

        Tab usage = new Tab("Usage");
        TableView<SummaryUsage> summaryUsageTableView = new TableView<>();
        summaryUsageTableView.setItems(summaryUsageObservableList);
        summaryUsageTableView.getColumns().addAll(createSummaryUsageColumns());
        usage.setContent(summaryUsageTableView);
        summaryTabPane.getTabs().add(usage);

        Tab party = new Tab("3rd party");
        TableView<SummaryPartyServices> summaryPartyServicesTableView = new TableView<>();
        summaryPartyServicesTableView.setItems(summaryPartyServicesObservableList);
        summaryPartyServicesTableView.getColumns().addAll(createSummaryPartyServicesColumns());
        party.setContent(summaryPartyServicesTableView);
        summaryTabPane.getTabs().add(party);

        Tab credit = new Tab("Credit");
        TableView<SummaryCredit> summaryCreditTableView = new TableView<>();
        summaryCreditTableView.setItems(summaryCreditObservableList);
        summaryCreditTableView.getColumns().addAll(createSummaryCreditColumns());
        credit.setContent(summaryCreditTableView);
        summaryTabPane.getTabs().add(credit);

        return summaryTabPane;
    }

    private TabPane createDetailTabPane(ObservableList<DetailHeader> detailHeaderObservableList,ObservableList<DetailData> detailDataObservableList){
        TabPane detailTabPane = new TabPane();
        Tab header = new Tab("Header");
        TableView<DetailHeader> headerTableView = new TableView<>();
        headerTableView.setItems(detailHeaderObservableList);
        headerTableView.getColumns().addAll(createDetailHeaderColumns());
        header.setContent(headerTableView);
        detailTabPane.getTabs().add(header);

        Tab data = new Tab("Data");
        TableView<DetailData> dataTableView = new TableView<>();
        dataTableView.setItems(detailDataObservableList);
        dataTableView.getColumns().addAll(createDetailDataColumns());
        data.setContent(dataTableView);
        detailTabPane.getTabs().add(data);

        return detailTabPane;
    }
}

