package com.o2.generatorCDR.controllers;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

public class SpringController implements ApplicationContextAware {
    private static ApplicationContext applicationContext = null;

    public  static ApplicationContext getApplicationContext(){
        return  applicationContext;
    }

    public void setApplicationContext(ApplicationContext context){
        applicationContext=context;
    }

}
