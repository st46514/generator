package com.o2.generatorCDR.controllers;

import com.o2.generatorCDR.service.CDR.CDRGenerator;
import com.o2.generatorCDR.stages.HashDialog;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.springframework.context.ApplicationContext;

import java.io.File;
import java.net.URL;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CDRM2MController implements Initializable {

    private CDRGenerator cdrGenerator;
    private ApplicationContext context;
    private StageController stageController;
    @FXML
    TextField msisdnTextField;

    @FXML
    TextField imsiTextField;

    @FXML
    TextField dataVolumeTextField;

    @FXML
    TextField roamingZoneTextField;

    @FXML
    TextField roamingCalledNumberTextField;

    @FXML
    TextField ipAddressTextField;

    @FXML
    Button generateAllButton;

    @FXML
    Button generateSpecificDataButton;

    @FXML
    Button generateSpecificXMLButton;

    @FXML
    ComboBox dataZoneComboBox;

    @FXML
    AnchorPane cdrAnchorPane;



    private SimpleDateFormat df;

    private static final Pattern VALID_MSISDN =
            Pattern.compile("^[0-9]{9,12}$", Pattern.CASE_INSENSITIVE);

    private static final Pattern VALID_IMSI =
            Pattern.compile("^[0-9]{15}$", Pattern.CASE_INSENSITIVE);

    private static final Pattern VALID_IP =
            Pattern.compile("^(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})$", Pattern.CASE_INSENSITIVE);

    @FXML
    public void handleMsisdnTextField(){

        checkTextFieldLength(msisdnTextField,20);

    }
    @FXML
    public void handleIMSITextField() {

        checkTextFieldLength(imsiTextField,16);

        if (msisdnTextField.getText().length() > 0 && imsiTextField.getText().length() > 0) {
            generateAllButton.disableProperty().setValue(false);

        } else {
            generateAllButton.disableProperty().setValue(true);

        }
    }

    @FXML
    public void handleDataVolume(){
        if (dataVolumeTextField.getText().length() > 0 && ipAddressTextField.getText().length() > 0 && msisdnTextField.getText().length() > 0 && imsiTextField.getText().length() > 0) {
            generateSpecificDataButton.disableProperty().setValue(false);

        } else {
            generateSpecificDataButton.disableProperty().setValue(true);

        }
    }

    @FXML
    public void handleSpecificRoaming(){
        if (roamingCalledNumberTextField.getText().length() > 0 && roamingZoneTextField.getText().length() > 0 && msisdnTextField.getText().length() > 0 && imsiTextField.getText().length() > 0) {
            generateSpecificXMLButton.disableProperty().setValue(false);

        } else {
            generateSpecificXMLButton.disableProperty().setValue(true);

        }
    }

    @FXML
    public void generateSpecificDataButton() {
        if (validateMsisdn(msisdnTextField.getText()) && validateImsi(imsiTextField.getText()) && validateIpAddress(ipAddressTextField.getText())) {
            File file = createFileChooser("*.lte");
            if (file != null) {
                try {
                    Long dataVolume = Long.parseLong(dataVolumeTextField.getText());
                    if (dataVolume > 19999999998L)
                        throw new NumberFormatException("Číslo je větší než je schopný zpracovat Billing.");
                    cdrGenerator.generateSpecificData(msisdnTextField.getText(), imsiTextField.getText(), ipAddressTextField.getText(), file.getAbsolutePath(), dataVolume, (String) dataZoneComboBox.getSelectionModel().getSelectedItem());
                } catch (NumberFormatException e) {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Error");
                    alert.setContentText("Objem dat nesmí obsahovat text!\n" + e.toString() + "\nNejvětší datový objem je 19999999998.");
                    alert.showAndWait();
                }
            }
        }else{
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setContentText("Zadaná čísla nejsou validní!");
            alert.showAndWait();
        }
    }

    @FXML
    public void generateSpecificXMLButton() {
        if (validateMsisdn(msisdnTextField.getText()) && validateImsi(imsiTextField.getText()) && validateMsisdn(roamingCalledNumberTextField.getText())) {
            File file = createFileChooser(".xml");
            if (file != null)
                cdrGenerator.generateSpecificXML(msisdnTextField.getText(), imsiTextField.getText(), file.getAbsolutePath(), roamingZoneTextField.getText().toUpperCase(), "CZEET", roamingCalledNumberTextField.getText());
        }else{
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setContentText("Zadaná čísla nejsou validní!");
            alert.showAndWait();
        }
    }

    @FXML
    public void handleSwitchScene() {
        stageController.setInvoiceScene();
    }

    public void handleCreateHashButton(){
        HashDialog hashDialog = new HashDialog(cdrAnchorPane.getScene().getWindow());
        hashDialog.showAndWait();
    }

    @FXML
    public void handleGenerateAllButton() {
        if (validateMsisdn(msisdnTextField.getText()) && validateImsi(imsiTextField.getText())) {
            File file = createFileChooser();
            String suffix = "";

            if (file != null) {
                int i = file.getName().lastIndexOf('.');
                if (i >= 0) {
                    suffix = file.getName().substring(i + 1);
                }
                switch (suffix) {
                    case "gvoice":
                        cdrGenerator.generateAllVoice(msisdnTextField.getText(), imsiTextField.getText(), file.getAbsolutePath());
                        break;
                    case "sms":
                        cdrGenerator.generateAllSMS(msisdnTextField.getText(), imsiTextField.getText(), file.getAbsolutePath());
                        break;
                    case "lte":
                        cdrGenerator.generateAllData(msisdnTextField.getText(), imsiTextField.getText(), file.getAbsolutePath());
                        break;
                    case "xml":
                        cdrGenerator.generateAllXML(msisdnTextField.getText(), imsiTextField.getText(), file.getAbsolutePath());
                        break;
                }

            }
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setContentText("Zadaná čísla nejsou validní MSISDN nebo IMSI!");
            alert.showAndWait();
        }
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        context = SpringController.getApplicationContext();
        cdrGenerator = context.getBean(CDRGenerator.class);
        stageController = context.getBean(StageController.class);
        df = new SimpleDateFormat("yyyyMMddHHmmss");
        ObservableList<String> roamingZones = FXCollections.observableArrayList("07", "05", "04");
        dataZoneComboBox.setItems(roamingZones);
        dataZoneComboBox.getSelectionModel().selectFirst();
        //   cdrGenerator.generateAllXML("420792007455","230029500027614","roaming");
        //   cdrGenerator.generateAllVoice("420792007455","230029500027614", "pmo_test125");
        //   cdrGenerator.generateAllData("420792007455","230029500027614","pmo_test126");
        //   cdrGenerator.generateAllSMS("420792007455","230029500027614","pmo_test127");

        //    cdrGenerator.generateSpecificXML("420792007455","230029500027614","roamingSpecific","SWEEP","CZEET","420720123123");
        //    cdrGenerator.generateSpecificData("420792007455","230029500027614","160.218.032.068","dataSpecific",2048,"07");

    }



    private void checkTextFieldLength(TextField textField, int number) {
        if (textField.getText().length() > number) {
            textField.setText(textField.getText().substring(0, number));
            textField.positionCaret(textField.getText().length());
        }

    }

    private File createFileChooser() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Save File");
        fileChooser.setInitialFileName(df.format(new Date().getTime()) + "_");
        String currentPath = Paths.get(".").toAbsolutePath().normalize().toString();
        fileChooser.setInitialDirectory(new File(currentPath));
        fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("Voice files", "*.gvoice")
                , new FileChooser.ExtensionFilter("SMS files", "*.sms")
                , new FileChooser.ExtensionFilter("Data files", "*.lte")
                , new FileChooser.ExtensionFilter("Roaming files", "*.xml"));
        return fileChooser.showSaveDialog(new Stage());
    }
    private File createFileChooser(String extension){
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Save File");
        fileChooser.setInitialFileName(df.format(new Date().getTime()) + "_");
        String currentPath = Paths.get(".").toAbsolutePath().normalize().toString();
        fileChooser.setInitialDirectory(new File(currentPath));
        fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("Voice files", extension));
        return fileChooser.showSaveDialog(new Stage());
    }

    public static boolean validateMsisdn(String MSISDN){
        Matcher matcher = VALID_MSISDN.matcher(MSISDN);
        return matcher.find();

    }

    private boolean validateImsi(String IMSI){
        Matcher matcher = VALID_IMSI.matcher(IMSI);
        return matcher.find();
    }

    private boolean validateIpAddress(String ipAddress){
        Matcher matcher = VALID_IP.matcher(ipAddress);
        return matcher.find();
    }


}



