package com.o2.generatorCDR.controllers;

import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class StageController {

    private Stage stage;
    private Scene invoiceScene;
    private Scene cdrM2MScene;

    public void init(Stage stage) {
        this.stage = stage;
        Group root = new Group();
        this.stage.setScene(new Scene(root));
    }

    public void setInvoiceScene(){
        try {
            invoiceScene = new Scene(FXMLLoader.load(getClass().getResource("/InvoiceScene.fxml")), 600, 300);
            stage.resizableProperty().setValue(true);
            stage.setTitle("Invoice CEZ");
            stage.setScene(invoiceScene);
            stage.show();

        }catch (IOException e){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setContentText(e.fillInStackTrace().getMessage());
            alert.showAndWait();
        }
    }

    public void setCdrM2MScene(){
        try {
            cdrM2MScene = new Scene(FXMLLoader.load(getClass().getResource("/CDRScene.fxml")), 600, 300);
            stage.resizableProperty().setValue(false);
            stage.setTitle("CDR M2M Generator");
            stage.setScene(cdrM2MScene);
            stage.show();
        }catch (IOException e){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setContentText(e.fillInStackTrace().getMessage());
            alert.showAndWait();
        }
    }


}
