package com.o2.generatorCDR.repository;

import org.dom4j.Document;

import java.io.IOException;

public interface FileManagement {

    void createCDRFile(String CDR, String fileName);

    void createXMLFile(Document document, String fileName, int numberOfFile);


}
