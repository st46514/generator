package com.o2.generatorCDR.repository;

import org.dom4j.Document;
import org.springframework.stereotype.Repository;
import org.springframework.util.StreamUtils;

import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Pattern;

@Repository
public class FileManagementImpl implements FileManagement {


    @Override
    public void createCDRFile(String CDR, String fileName) {
        try {
            OutputStream outputStream = new FileOutputStream(fileName);
            StreamUtils.copy(CDR, StandardCharsets.UTF_8,outputStream);
            outputStream.close();

        }catch (IOException e){
            e.fillInStackTrace();
        }
    }

    @Override
    public void createXMLFile(Document document, String fileName,int numberOfFile) {
        try {
            String[] strings = fileName.split(Pattern.quote("."));
            fileName = fileName.join(String.valueOf(numberOfFile) + ".",strings);
            FileWriter out = new FileWriter(fileName);
            document.write(out);
            out.close();
        }catch (IOException e) {
        e.fillInStackTrace();
        }
    }


}
