package com.o2.generatorCDR.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.o2.generatorCDR")
public class AppConfig {

}
