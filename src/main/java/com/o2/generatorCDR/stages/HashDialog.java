package com.o2.generatorCDR.stages;

import com.o2.generatorCDR.controllers.CDRM2MController;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;
import sun.misc.BASE64Encoder;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


public class HashDialog extends Stage {

    final  String KEY = "SecretKeyForRestApiTesco12345678";
    final  String MSISDN = "msisdn:";
    final String DATE_FORMAT = "EEE, dd MMM yyyy HH:mm:ss zzz";
    final SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT, Locale.US);


    public HashDialog(Window window) {
        setTitle("Vytvoř Hash");
        setWidth(350);
        setHeight(200);

        initStyle(StageStyle.UTILITY);
        initModality(Modality.WINDOW_MODAL);
        initOwner(window);
        setScene(createScene());
    }

    private Scene createScene(){
        VBox box = new VBox();
        box.setAlignment(Pos.CENTER);
        box.setSpacing(10);


        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setPadding(new Insets(10));
        grid.setHgap(10);
        grid.setVgap(10);


        TextField msisdnTextField = new TextField();
        TextField clientIdTextField = new TextField();
        TextField datumTextField = new TextField();
        Label msisdnLabel = new Label("MSISDN/heslo:");
        Label clientIdLabel = new Label("Client Id:");
        Label dateLabel = new Label("Datum");
        clientIdTextField.setEditable(false);
        datumTextField.setEditable(false);
        grid.add(msisdnLabel, 0, 0);
        grid.add(msisdnTextField, 1, 0);
        grid.add(dateLabel,0,2);
        grid.add(clientIdLabel, 0, 1);
        grid.add(clientIdTextField, 1, 1);
        grid.add(datumTextField,1,2);


        Button buttonHash = new Button("Vytvoř hash hlavičky");
        buttonHash.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                StringBuilder stringBuilder = new StringBuilder();

                String datum = sdf.format(new Date().getTime());

                try {
                    if (CDRM2MController.validateMsisdn(msisdnTextField.getText())) {
                        String hash = hmacSha1(stringBuilder.append(datum).append(MSISDN).append(msisdnTextField.getText()).toString(), KEY);

                        clientIdTextField.setText("ClientId:" + hash);
                        datumTextField.setText(datum);
                    }else{
                        Alert alert = new Alert(Alert.AlertType.ERROR);
                        alert.setTitle("Error");
                        alert.setContentText("Zadaná čísla nejsou validní!");
                        alert.showAndWait();
                    }
                }catch (Exception ex){
                    ex.fillInStackTrace();
                }
            }
        });

        Button buttonHashPassword = new Button("Vytvoř hash hesla");
        buttonHashPassword.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {




                try {
                    String hash = hashString(msisdnTextField.getText());
                    clientIdTextField.setText(hash);

                    }catch (Exception ex){
                    ex.fillInStackTrace();
                }
            }
        });
grid.add(buttonHash,0,3);
grid.add(buttonHashPassword,1,3);
        box.getChildren().addAll(grid);
        return new Scene(box);
    }

    private static String hmacSha1(String value, String key) throws NoSuchAlgorithmException, InvalidKeyException {
        SecretKeySpec keySpec = new SecretKeySpec(key.getBytes(), "HmacSHA1");

        Mac mac = Mac.getInstance("HmacSHA1");
        mac.init(keySpec);
        byte[] result = mac.doFinal(value.getBytes());

        BASE64Encoder encoder = new BASE64Encoder();
        return encoder.encode(result);
    }

    private static String hashString(String strToHash) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        if (strToHash == null) {
            return "";
        }

        MessageDigest crypt = MessageDigest.getInstance("SHA-1");
        byte[] digest = crypt.digest(strToHash.getBytes());
        String hash = new BigInteger(1, digest).toString(16);


        return hash;
    }
}
