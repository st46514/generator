package com.o2.generatorCDR.entity;

public class DetailData {
    private String outputType;
    private String subscriptionId;
    private String lineType;
    private String recordRefereceId;
    private String serviceCategory;
    private String serviceCategoryName;
    private String chargingCode;
    private String chargingCodeName;
    private String serviceID;
    private String serviceName;
    private String bNumber;
    private String startTime;
    private String duration;
    private String events;
    private String VolumeDownloaded;
    private String VolumeUploaded;
    private String units;
    private String freeUnits;
    private String priceBefore;
    private String priceAfter;
    private String VATCode;
    private String VATPrice;
    private String finalPrice;
    private String wholeSalePrice;
    private String timeZone;

    public String getOutputType() {
        return outputType;
    }

    public void setOutputType(String outputType) {
        this.outputType = outputType;
    }

    public String getSubscriptionId() {
        return subscriptionId;
    }

    public void setSubscriptionId(String subscriptionId) {
        this.subscriptionId = subscriptionId;
    }

    public String getLineType() {
        return lineType;
    }

    public void setLineType(String lineType) {
        this.lineType = lineType;
    }

    public String getRecordRefereceId() {
        return recordRefereceId;
    }

    public void setRecordRefereceId(String recordRefereceId) {
        this.recordRefereceId = recordRefereceId;
    }

    public String getServiceCategory() {
        return serviceCategory;
    }

    public void setServiceCategory(String serviceCategory) {
        this.serviceCategory = serviceCategory;
    }

    public String getServiceCategoryName() {
        return serviceCategoryName;
    }

    public void setServiceCategoryName(String serviceCategoryName) {
        this.serviceCategoryName = serviceCategoryName;
    }

    public String getChargingCode() {
        return chargingCode;
    }

    public void setChargingCode(String chargingCode) {
        this.chargingCode = chargingCode;
    }

    public String getChargingCodeName() {
        return chargingCodeName;
    }

    public void setChargingCodeName(String chargingCodeName) {
        this.chargingCodeName = chargingCodeName;
    }

    public String getServiceID() {
        return serviceID;
    }

    public void setServiceID(String serviceID) {
        this.serviceID = serviceID;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getbNumber() {
        return bNumber;
    }

    public void setbNumber(String bNumber) {
        this.bNumber = bNumber;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getEvents() {
        return events;
    }

    public void setEvents(String events) {
        this.events = events;
    }

    public String getVolumeDownloaded() {
        return VolumeDownloaded;
    }

    public void setVolumeDownloaded(String volumeDownloaded) {
        VolumeDownloaded = volumeDownloaded;
    }

    public String getVolumeUploaded() {
        return VolumeUploaded;
    }

    public void setVolumeUploaded(String volumeUploaded) {
        VolumeUploaded = volumeUploaded;
    }

    public String getUnits() {
        return units;
    }

    public void setUnits(String units) {
        this.units = units;
    }

    public String getFreeUnits() {
        return freeUnits;
    }

    public void setFreeUnits(String freeUnits) {
        this.freeUnits = freeUnits;
    }

    public String getPriceBefore() {
        return priceBefore;
    }

    public void setPriceBefore(String priceBefore) {
        this.priceBefore = priceBefore;
    }

    public String getPriceAfter() {
        return priceAfter;
    }

    public void setPriceAfter(String priceAfter) {
        this.priceAfter = priceAfter;
    }

    public String getVATCode() {
        return VATCode;
    }

    public void setVATCode(String VATCode) {
        this.VATCode = VATCode;
    }

    public String getVATPrice() {
        return VATPrice;
    }

    public void setVATPrice(String VATPrice) {
        this.VATPrice = VATPrice;
    }

    public String getFinalPrice() {
        return finalPrice;
    }

    public void setFinalPrice(String finalPrice) {
        this.finalPrice = finalPrice;
    }

    public String getWholeSalePrice() {
        return wholeSalePrice;
    }

    public void setWholeSalePrice(String wholeSalePrice) {
        this.wholeSalePrice = wholeSalePrice;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    public DetailData(String outputType, String subscriptionId, String lineType, String recordRefereceId, String serviceCategory, String serviceCategoryName, String chargingCode, String chargingCodeName, String serviceID, String serviceName, String bNumber, String startTime, String duration, String events, String volumeDownloaded, String volumeUploaded, String units, String freeUnits, String priceBefore, String priceAfter, String VATCode, String VATPrice, String finalPrice, String wholeSalePrice, String timeZone) {
        this.outputType = outputType;
        this.subscriptionId = subscriptionId;
        this.lineType = lineType;
        this.recordRefereceId = recordRefereceId;
        this.serviceCategory = serviceCategory;
        this.serviceCategoryName = serviceCategoryName;
        this.chargingCode = chargingCode;
        this.chargingCodeName = chargingCodeName;
        this.serviceID = serviceID;
        this.serviceName = serviceName;
        this.bNumber = bNumber;
        this.startTime = startTime;
        this.duration = duration;
        this.events = events;
        VolumeDownloaded = volumeDownloaded;
        VolumeUploaded = volumeUploaded;
        this.units = units;
        this.freeUnits = freeUnits;
        this.priceBefore = priceBefore;
        this.priceAfter = priceAfter;
        this.VATCode = VATCode;
        this.VATPrice = VATPrice;
        this.finalPrice = finalPrice;
        this.wholeSalePrice = wholeSalePrice;
        this.timeZone = timeZone;
    }
}
