package com.o2.generatorCDR.entity;

public class InvoiceData {

    private String outputType;
    private String payerReference;
    private String lineType;
    private String serviceCategory;
    private String priceWithoutVAT;
    private String VATCode;
    private String VATPrice;
    private String priceIncludingVAT;

    public InvoiceData(String outputType, String payerReference, String lineType, String serviceCategory, String priceWithoutVAT, String VATCode, String VATPrice, String priceIncludingVAT) {
        this.outputType = outputType;
        this.payerReference = payerReference;
        this.lineType = lineType;
        this.serviceCategory = serviceCategory;
        this.priceWithoutVAT = priceWithoutVAT;
        this.VATCode = VATCode;
        this.VATPrice = VATPrice;
        this.priceIncludingVAT = priceIncludingVAT;
    }

    public String getOutputType() {
        return outputType;
    }

    public void setOutputType(String outputType) {
        this.outputType = outputType;
    }

    public String getPayerReference() {
        return payerReference;
    }

    public void setPayerReference(String payerReference) {
        this.payerReference = payerReference;
    }

    public String getLineType() {
        return lineType;
    }

    public void setLineType(String lineType) {
        this.lineType = lineType;
    }

    public String getServiceCategory() {
        return serviceCategory;
    }

    public void setServiceCategory(String serviceCategory) {
        this.serviceCategory = serviceCategory;
    }

    public String getPriceWithoutVAT() {
        return priceWithoutVAT;
    }

    public void setPriceWithoutVAT(String priceWithoutVAT) {
        this.priceWithoutVAT = priceWithoutVAT;
    }

    public String getVATCode() {
        return VATCode;
    }

    public void setVATCode(String VATCode) {
        this.VATCode = VATCode;
    }

    public String getVATPrice() {
        return VATPrice;
    }

    public void setVATPrice(String VATPrice) {
        this.VATPrice = VATPrice;
    }

    public String getPriceIncludingVAT() {
        return priceIncludingVAT;
    }

    public void setPriceIncludingVAT(String priceIncludingVAT) {
        this.priceIncludingVAT = priceIncludingVAT;
    }
}
