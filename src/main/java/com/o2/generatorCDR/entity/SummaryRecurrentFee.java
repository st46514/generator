package com.o2.generatorCDR.entity;

public class SummaryRecurrentFee {

    private String outputType;
    private String subscriptionId;
    private String lineType;
    private String serviceID;
    private String serviceName;
    private String chargingCode;
    private String chargingCodeName;
    private String startDate;
    private String endDate;
    private String amount;
    private String sumPriceBefore;
    private String sumPriceAfter;
    private String sumDiscount;
    private String VATCode;
    private String sumVATPrice;
    private String sumFinalPrice;
    private String sumWholeSalePrice;


    public String getOutputType() {
        return outputType;
    }

    public void setOutputType(String outputType) {
        this.outputType = outputType;
    }

    public String getSubscriptionId() {
        return subscriptionId;
    }

    public void setSubscriptionId(String subscriptionId) {
        this.subscriptionId = subscriptionId;
    }

    public String getLineType() {
        return lineType;
    }

    public void setLineType(String lineType) {
        this.lineType = lineType;
    }

    public String getServiceID() {
        return serviceID;
    }

    public void setServiceID(String serviceID) {
        this.serviceID = serviceID;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getChargingCode() {
        return chargingCode;
    }

    public void setChargingCode(String chargingCode) {
        this.chargingCode = chargingCode;
    }

    public String getChargingCodeName() {
        return chargingCodeName;
    }

    public void setChargingCodeName(String chargingCodeName) {
        this.chargingCodeName = chargingCodeName;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getSumPriceBefore() {
        return sumPriceBefore;
    }

    public void setSumPriceBefore(String sumPriceBefore) {
        this.sumPriceBefore = sumPriceBefore;
    }

    public String getSumPriceAfter() {
        return sumPriceAfter;
    }

    public void setSumPriceAfter(String sumPriceAfter) {
        this.sumPriceAfter = sumPriceAfter;
    }

    public String getSumDiscount() {
        return sumDiscount;
    }

    public void setSumDiscount(String sumDiscount) {
        this.sumDiscount = sumDiscount;
    }

    public String getVATCode() {
        return VATCode;
    }

    public void setVATCode(String VATCode) {
        this.VATCode = VATCode;
    }

    public String getSumVATPrice() {
        return sumVATPrice;
    }

    public void setSumVATPrice(String sumVATPrice) {
        this.sumVATPrice = sumVATPrice;
    }

    public String getSumFinalPrice() {
        return sumFinalPrice;
    }

    public void setSumFinalPrice(String sumFinalPrice) {
        this.sumFinalPrice = sumFinalPrice;
    }

    public String getSumWholeSalePrice() {
        return sumWholeSalePrice;
    }

    public void setSumWholeSalePrice(String sumWholeSalePrice) {
        this.sumWholeSalePrice = sumWholeSalePrice;
    }

    public SummaryRecurrentFee(String outputType, String subscriptionId, String lineType, String serviceID, String serviceName, String chargingCode, String chargingCodeName, String startDate, String endDate, String amount, String sumPriceBefore, String sumPriceAfter, String sumDiscount, String VATCode, String sumVATPrice, String sumFinalPrice, String sumWholeSalePrice) {
        this.outputType = outputType;
        this.subscriptionId = subscriptionId;
        this.lineType = lineType;
        this.serviceID = serviceID;
        this.serviceName = serviceName;
        this.chargingCode = chargingCode;
        this.chargingCodeName = chargingCodeName;
        this.startDate = startDate;
        this.endDate = endDate;
        this.amount = amount;
        this.sumPriceBefore = sumPriceBefore;
        this.sumPriceAfter = sumPriceAfter;
        this.sumDiscount = sumDiscount;
        this.VATCode = VATCode;
        this.sumVATPrice = sumVATPrice;
        this.sumFinalPrice = sumFinalPrice;
        this.sumWholeSalePrice = sumWholeSalePrice;
    }



}
