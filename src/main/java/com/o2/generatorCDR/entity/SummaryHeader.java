package com.o2.generatorCDR.entity;

public class SummaryHeader {
    private String outputType;
    private String subscriptionId;
    private String lineType;
    private String msisdn;
    private String extSubscriptionId;
    private String currencyCode;
    private String billingPeriodStart;
    private String billingPeriodEnd;
    private String payerReference;
    private String extPayerId;
    private String tariffName;

    public String getOutputType() {
        return outputType;
    }

    public void setOutputType(String outputType) {
        this.outputType = outputType;
    }

    public String getSubscriptionId() {
        return subscriptionId;
    }

    public void setSubscriptionId(String subscriptionId) {
        this.subscriptionId = subscriptionId;
    }

    public String getLineType() {
        return lineType;
    }

    public void setLineType(String lineType) {
        this.lineType = lineType;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getExtSubscriptionId() {
        return extSubscriptionId;
    }

    public void setExtSubscriptionId(String extSubscriptionId) {
        this.extSubscriptionId = extSubscriptionId;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getBillingPeriodStart() {
        return billingPeriodStart;
    }

    public void setBillingPeriodStart(String billingPeriodStart) {
        this.billingPeriodStart = billingPeriodStart;
    }

    public String getBillingPeriodEnd() {
        return billingPeriodEnd;
    }

    public void setBillingPeriodEnd(String billingPeriodEnd) {
        this.billingPeriodEnd = billingPeriodEnd;
    }

    public String getPayerReference() {
        return payerReference;
    }

    public void setPayerReference(String payerReference) {
        this.payerReference = payerReference;
    }

    public String getExtPayerId() {
        return extPayerId;
    }

    public void setExtPayerId(String extPayerId) {
        this.extPayerId = extPayerId;
    }

    public String getTariffName() {
        return tariffName;
    }

    public void setTariffName(String tariffName) {
        this.tariffName = tariffName;
    }

    public SummaryHeader(String outputType, String subscriptionId, String lineType, String msisdn, String extSubscriptionId, String currencyCode, String billingPeriodStart, String billingPeriodEnd, String payerReference, String extPayerId, String tariffName) {
        this.outputType = outputType;
        this.subscriptionId = subscriptionId;
        this.lineType = lineType;
        this.msisdn = msisdn;
        this.extSubscriptionId = extSubscriptionId;
        this.currencyCode = currencyCode;
        this.billingPeriodStart = billingPeriodStart;
        this.billingPeriodEnd = billingPeriodEnd;
        this.payerReference = payerReference;
        this.extPayerId = extPayerId;
        this.tariffName = tariffName;
    }
}
