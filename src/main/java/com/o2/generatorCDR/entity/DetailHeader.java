package com.o2.generatorCDR.entity;

public class DetailHeader {
    private String outputType;
    private String subscriptionId;
    private String lineType;
    private String msisdn;
    private String extSubscriptionId;
    private String currencyCode;

    public String getOutputType() {
        return outputType;
    }

    public void setOutputType(String outputType) {
        this.outputType = outputType;
    }

    public String getSubscriptionId() {
        return subscriptionId;
    }

    public void setSubscriptionId(String subscriptionId) {
        this.subscriptionId = subscriptionId;
    }

    public String getLineType() {
        return lineType;
    }

    public void setLineType(String lineType) {
        this.lineType = lineType;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getExtSubscriptionId() {
        return extSubscriptionId;
    }

    public void setExtSubscriptionId(String extSubscriptionId) {
        this.extSubscriptionId = extSubscriptionId;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public DetailHeader(String outputType, String subscriptionId, String lineType, String msisdn, String extSubscriptionId, String currencyCode) {
        this.outputType = outputType;
        this.subscriptionId = subscriptionId;
        this.lineType = lineType;
        this.msisdn = msisdn;
        this.extSubscriptionId = extSubscriptionId;
        this.currencyCode = currencyCode;
    }
}
