package com.o2.generatorCDR.entity;

public class InvoiceHeader {

    private String outputType;
    private String payerReference;
    private String lineType;
    private String currencyCode;
    private String billingPeriodStart;
    private String billingPeriodEnd;
    private String extPayerId;
    private String finalPriceWithoutVAT;
    private String VATPrice;
    private String finalPriceIncludingVAT;

    public InvoiceHeader(String outputType, String payerReference, String lineType, String currencyCode, String billingPeriodStart, String billingPeriodEnd, String extPayerId, String finalPriceWithoutVAT, String VATPrice, String finalPriceIncludingVAT) {
        this.outputType = outputType;
        this.payerReference = payerReference;
        this.lineType = lineType;
        this.currencyCode = currencyCode;
        this.billingPeriodStart = billingPeriodStart;
        this.billingPeriodEnd = billingPeriodEnd;
        this.extPayerId = extPayerId;
        this.finalPriceWithoutVAT = finalPriceWithoutVAT;
        this.VATPrice = VATPrice;
        this.finalPriceIncludingVAT = finalPriceIncludingVAT;
    }

    public String getOutputType() {
        return outputType;
    }

    public void setOutputType(String outputType) {
        this.outputType = outputType;
    }

    public String getPayerReference() {
        return payerReference;
    }

    public void setPayerReference(String payerReference) {
        this.payerReference = payerReference;
    }

    public String getLineType() {
        return lineType;
    }

    public void setLineType(String lineType) {
        this.lineType = lineType;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getBillingPeriodStart() {
        return billingPeriodStart;
    }

    public void setBillingPeriodStart(String billingPeriodStart) {
        this.billingPeriodStart = billingPeriodStart;
    }

    public String getBillingPeriodEnd() {
        return billingPeriodEnd;
    }

    public void setBillingPeriodEnd(String billingPeriodEnd) {
        this.billingPeriodEnd = billingPeriodEnd;
    }

    public String getExtPayerId() {
        return extPayerId;
    }

    public void setExtPayerId(String extPayerId) {
        this.extPayerId = extPayerId;
    }

    public String getFinalPriceWithoutVAT() {
        return finalPriceWithoutVAT;
    }

    public void setFinalPriceWithoutVAT(String finalPriceWithoutVAT) {
        this.finalPriceWithoutVAT = finalPriceWithoutVAT;
    }

    public String getVATPrice() {
        return VATPrice;
    }

    public void setVATPrice(String VATPrice) {
        this.VATPrice = VATPrice;
    }

    public String getFinalPriceIncludingVAT() {
        return finalPriceIncludingVAT;
    }

    public void setFinalPriceIncludingVAT(String finalPriceIncludingVAT) {
        this.finalPriceIncludingVAT = finalPriceIncludingVAT;
    }
}
