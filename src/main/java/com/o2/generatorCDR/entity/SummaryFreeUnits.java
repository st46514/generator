package com.o2.generatorCDR.entity;

public class SummaryFreeUnits {
    private String outputType;
    private String subscriptionId;
    private String lineType;
    private String counterCode;
    private String unitType;
    private String InitialCounterValue;
    private String used;
    private String transferred;
    private String rollover;

    public String getOutputType() {
        return outputType;
    }

    public void setOutputType(String outputType) {
        this.outputType = outputType;
    }

    public String getSubscriptionId() {
        return subscriptionId;
    }

    public void setSubscriptionId(String subscriptionId) {
        this.subscriptionId = subscriptionId;
    }

    public String getLineType() {
        return lineType;
    }

    public void setLineType(String lineType) {
        this.lineType = lineType;
    }

    public String getCounterCode() {
        return counterCode;
    }

    public void setCounterCode(String counterCode) {
        this.counterCode = counterCode;
    }

    public String getUnitType() {
        return unitType;
    }

    public void setUnitType(String unitType) {
        this.unitType = unitType;
    }

    public String getInitialCounterValue() {
        return InitialCounterValue;
    }

    public void setInitialCounterValue(String initialCounterValue) {
        InitialCounterValue = initialCounterValue;
    }

    public String getUsed() {
        return used;
    }

    public void setUsed(String used) {
        this.used = used;
    }

    public String getTransferred() {
        return transferred;
    }

    public void setTransferred(String transferred) {
        this.transferred = transferred;
    }

    public String getRollover() {
        return rollover;
    }

    public void setRollover(String rollover) {
        this.rollover = rollover;
    }

    public SummaryFreeUnits(String outputType, String subscriptionId, String lineType, String counterCode, String unitType, String initialCounterValue, String used, String transferred, String rollover) {
        this.outputType = outputType;
        this.subscriptionId = subscriptionId;
        this.lineType = lineType;
        this.counterCode = counterCode;
        this.unitType = unitType;
        InitialCounterValue = initialCounterValue;
        this.used = used;
        this.transferred = transferred;
        this.rollover = rollover;
    }
}
