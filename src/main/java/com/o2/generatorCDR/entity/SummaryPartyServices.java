package com.o2.generatorCDR.entity;

public class SummaryPartyServices {
    private String outputType;
    private String subscriptionId;
    private String lineType;
    private String serviceCategory;
    private String serviceCategoryName;
    private String chargingCode;
    private String chargingCodeName;
    private String SumDuration;
    private String sumEvents;
    private String sumVolumeDownloaded;
    private String sumVolumeUploaded;
    private String sumUnits;
    private String sumFreeUnits;
    private String sumPriceAfter;
    private String VATCode;
    private String sumVATPrice;
    private String sumFinalPrice;
    private String sumWholeSalePrice;

    public String getOutputType() {
        return outputType;
    }

    public void setOutputType(String outputType) {
        this.outputType = outputType;
    }

    public String getSubscriptionId() {
        return subscriptionId;
    }

    public void setSubscriptionId(String subscriptionId) {
        this.subscriptionId = subscriptionId;
    }

    public String getLineType() {
        return lineType;
    }

    public void setLineType(String lineType) {
        this.lineType = lineType;
    }

    public String getServiceCategory() {
        return serviceCategory;
    }

    public void setServiceCategory(String serviceCategory) {
        this.serviceCategory = serviceCategory;
    }

    public String getServiceCategoryName() {
        return serviceCategoryName;
    }

    public void setServiceCategoryName(String serviceCategoryName) {
        this.serviceCategoryName = serviceCategoryName;
    }

    public String getChargingCode() {
        return chargingCode;
    }

    public void setChargingCode(String chargingCode) {
        this.chargingCode = chargingCode;
    }

    public String getChargingCodeName() {
        return chargingCodeName;
    }

    public void setChargingCodeName(String chargingCodeName) {
        this.chargingCodeName = chargingCodeName;
    }

    public String getSumDuration() {
        return SumDuration;
    }

    public void setSumDuration(String sumDuration) {
        SumDuration = sumDuration;
    }

    public String getSumEvents() {
        return sumEvents;
    }

    public void setSumEvents(String sumEvents) {
        this.sumEvents = sumEvents;
    }

    public String getSumVolumeDownloaded() {
        return sumVolumeDownloaded;
    }

    public void setSumVolumeDownloaded(String sumVolumeDownloaded) {
        this.sumVolumeDownloaded = sumVolumeDownloaded;
    }

    public String getSumVolumeUploaded() {
        return sumVolumeUploaded;
    }

    public void setSumVolumeUploaded(String sumVolumeUploaded) {
        this.sumVolumeUploaded = sumVolumeUploaded;
    }

    public String getSumUnits() {
        return sumUnits;
    }

    public void setSumUnits(String sumUnits) {
        this.sumUnits = sumUnits;
    }

    public String getSumFreeUnits() {
        return sumFreeUnits;
    }

    public void setSumFreeUnits(String sumFreeUnits) {
        this.sumFreeUnits = sumFreeUnits;
    }

    public String getSumPriceAfter() {
        return sumPriceAfter;
    }

    public void setSumPriceAfter(String sumPriceAfter) {
        this.sumPriceAfter = sumPriceAfter;
    }

    public String getVATCode() {
        return VATCode;
    }

    public void setVATCode(String VATCode) {
        this.VATCode = VATCode;
    }

    public String getSumVATPrice() {
        return sumVATPrice;
    }

    public void setSumVATPrice(String sumVATPrice) {
        this.sumVATPrice = sumVATPrice;
    }

    public String getSumFinalPrice() {
        return sumFinalPrice;
    }

    public void setSumFinalPrice(String sumFinalPrice) {
        this.sumFinalPrice = sumFinalPrice;
    }

    public String getSumWholeSalePrice() {
        return sumWholeSalePrice;
    }

    public void setSumWholeSalePrice(String sumWholeSalePrice) {
        this.sumWholeSalePrice = sumWholeSalePrice;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    public SummaryPartyServices(String outputType, String subscriptionId, String lineType, String serviceCategory, String serviceCategoryName, String chargingCode, String chargingCodeName, String sumDuration, String sumEvents, String sumVolumeDownloaded, String sumVolumeUploaded, String sumUnits, String sumFreeUnits, String sumPriceAfter, String VATCode, String sumVATPrice, String sumFinalPrice, String sumWholeSalePrice, String timeZone) {
        this.outputType = outputType;
        this.subscriptionId = subscriptionId;
        this.lineType = lineType;
        this.serviceCategory = serviceCategory;
        this.serviceCategoryName = serviceCategoryName;
        this.chargingCode = chargingCode;
        this.chargingCodeName = chargingCodeName;
        SumDuration = sumDuration;
        this.sumEvents = sumEvents;
        this.sumVolumeDownloaded = sumVolumeDownloaded;
        this.sumVolumeUploaded = sumVolumeUploaded;
        this.sumUnits = sumUnits;
        this.sumFreeUnits = sumFreeUnits;
        this.sumPriceAfter = sumPriceAfter;
        this.VATCode = VATCode;
        this.sumVATPrice = sumVATPrice;
        this.sumFinalPrice = sumFinalPrice;
        this.sumWholeSalePrice = sumWholeSalePrice;
        this.timeZone = timeZone;
    }

    private String timeZone;
}
