package com.o2.generatorCDR.entity;

public class SummaryCredit {
    private String outputType;
    private String subscriptionId;
    private String lineType;
    private String itemType;
    private String chargingCode;
    private String serviceName;
    private String amount;
    private String sumPriceAfter;
    private String VATCode;
    private String sumVATPrice;
    private String sumFinalPrice;
    private String sumWholeSalePrice;

    public String getOutputType() {
        return outputType;
    }

    public void setOutputType(String outputType) {
        this.outputType = outputType;
    }

    public String getSubscriptionId() {
        return subscriptionId;
    }

    public void setSubscriptionId(String subscriptionId) {
        this.subscriptionId = subscriptionId;
    }

    public String getLineType() {
        return lineType;
    }

    public void setLineType(String lineType) {
        this.lineType = lineType;
    }

    public String getItemType() {
        return itemType;
    }

    public void setItemType(String itemType) {
        this.itemType = itemType;
    }

    public String getChargingCode() {
        return chargingCode;
    }

    public void setChargingCode(String chargingCode) {
        this.chargingCode = chargingCode;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getSumPriceAfter() {
        return sumPriceAfter;
    }

    public void setSumPriceAfter(String sumPriceAfter) {
        this.sumPriceAfter = sumPriceAfter;
    }

    public String getVATCode() {
        return VATCode;
    }

    public void setVATCode(String VATCode) {
        this.VATCode = VATCode;
    }

    public String getSumVATPrice() {
        return sumVATPrice;
    }

    public void setSumVATPrice(String sumVATPrice) {
        this.sumVATPrice = sumVATPrice;
    }

    public String getSumFinalPrice() {
        return sumFinalPrice;
    }

    public void setSumFinalPrice(String sumFinalPrice) {
        this.sumFinalPrice = sumFinalPrice;
    }

    public String getSumWholeSalePrice() {
        return sumWholeSalePrice;
    }

    public void setSumWholeSalePrice(String sumWholeSalePrice) {
        this.sumWholeSalePrice = sumWholeSalePrice;
    }

    public SummaryCredit(String outputType, String subscriptionId, String lineType, String itemType, String chargingCode, String serviceName, String amount, String sumPriceAfter, String VATCode, String sumVATPrice, String sumFinalPrice, String sumWholeSalePrice) {
        this.outputType = outputType;
        this.subscriptionId = subscriptionId;
        this.lineType = lineType;
        this.itemType = itemType;
        this.chargingCode = chargingCode;
        this.serviceName = serviceName;
        this.amount = amount;
        this.sumPriceAfter = sumPriceAfter;
        this.VATCode = VATCode;
        this.sumVATPrice = sumVATPrice;
        this.sumFinalPrice = sumFinalPrice;
        this.sumWholeSalePrice = sumWholeSalePrice;
    }
}
