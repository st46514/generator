package com.o2.generatorCDR.service.CDR;

import org.dom4j.Document;

public interface CDRCreator{

    String createSMSCDR(String callingNumber, String IMSI, String calledNumber, String vmscNumber, String tariffClass);

    String createDataCDR(String callingNumber, String IMSI, String IPAddress, Long dataValue, String roamingIdentification);

    String createVoiceCDR(String callingNumber, String IMSI, String calledNumber, String tariffClass);

    Document createXML(String callingNumber, String IMSI, String senderKey, String receiverKey, String[] calledNumbers);
}
