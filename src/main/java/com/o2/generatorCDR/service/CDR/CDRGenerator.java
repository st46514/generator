package com.o2.generatorCDR.service.CDR;

import org.dom4j.Document;

import java.io.FileWriter;

public interface CDRGenerator {




    void generateAllSMS(String callingNumber,String IMSI, String fileName);

    void generateAllData(String callingNumber,String IMSI, String fileName);

    void generateSpecificData(String callingNumber, String IMSI,String IPAddress,String fileName ,Long dataValue, String roamingIdentification);

    void generateAllVoice(String callingNumber,String IMSI, String fileName);

    void generateAllXML(String callingNumber, String IMSI, String fileName);

    void generateSpecificXML(String callingNumber, String IMSI, String fileName, String sender, String receiver, String calledNumber);

  }
