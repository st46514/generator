package com.o2.generatorCDR.service.CDR;

import com.o2.generatorCDR.repository.FileManagement;
import org.dom4j.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CDRGeneratorImpl implements CDRGenerator {
    @Autowired
    FileManagement fileManagement;

    @Autowired
    CDRCreator cdrCreator;


    @Override
    public void generateAllXML(String callingNumber, String IMSI, String fileName) {
        String[] recipientKeys = {"SWEEP", "CHNCT", "BLRMD"};
        String[] calledNumbers = {"460123123123", "355123123123", "100123123123"};
        int i = 0;

        for (String string : recipientKeys) {
            i++;
            Document document = cdrCreator.createXML(callingNumber, IMSI, string, "CZEET", calledNumbers);
            fileManagement.createXMLFile(document,fileName,i);
        }
    }

    @Override
    public void generateSpecificXML(String callingNumber, String IMSI, String fileName, String sender, String receiver, String calledNumber) {
        String[] calledNumbers = {calledNumber};

        Document document = cdrCreator.createXML(callingNumber,IMSI,sender,receiver,calledNumbers);
        fileManagement.createXMLFile(document,fileName,0);

    }

    @Override
    public void generateAllSMS(String callingNumber, String IMSI, String fileName) {
        String[] calledNumbers={"420602302522","420602302522","420602302522","420602302522","420602302522","420602302522","420602302522","420602302522","420602302522","420602302522","420602302522","420602302522"
                ,"420223456789","4201180","4201181","4201188","4201224","4201211","42012123","42013123","42014112","42014234","420606000606","420800000606","420158","420116123","42099962030"
                ,"420813456789" ,"420841456789","420823456789","420913456789","420933456789","420953456789","420893456789","447537410297","420602302522","460123123123","355123123123"
                ,"100123123123","460123123123","355123123123","100123123123","460123123123","355123123123","100123123123","460602302522","355602302522","100602302522"};
        String[] vmscNumbers = {callingNumber,callingNumber,callingNumber,callingNumber,callingNumber,callingNumber,callingNumber,callingNumber,callingNumber,callingNumber,callingNumber
                ,callingNumber,callingNumber,callingNumber,callingNumber,callingNumber,callingNumber,callingNumber,callingNumber,callingNumber,callingNumber
                ,callingNumber,callingNumber,callingNumber,callingNumber,callingNumber,callingNumber,callingNumber,callingNumber,callingNumber,callingNumber
                ,callingNumber,callingNumber,callingNumber,callingNumber,"460602020000","460602020000","460602020000","460602020000","355602020000","355602020000","355602020000"
                ,"120602020000","120602020000","120602020000",callingNumber,callingNumber,callingNumber};
        String[] tariffClasses = {"2301","2302","2303","2304","2305","2306","2307","2308","2310","2330","2331","2300","2300","2300","2300","2300","2300","2300","2300","2300","2300","2300","2300"
                ,"2300","2300","2300","2300","2300","2300","2300","2300","2300","2300","2300","2300","2302","2302","2302","2302","2302","2302","2302","2302","2302","2302","2302","2302","2302"};
        StringBuilder stringBuilder = new StringBuilder();

        for (int i = 0; i < calledNumbers.length; i++) {
            stringBuilder.append(cdrCreator.createSMSCDR(callingNumber,IMSI,calledNumbers[i],vmscNumbers[i],tariffClasses[i])).append("\n");
        }
        fileManagement.createCDRFile(stringBuilder.toString(),fileName);
    }


    @Override
    public void generateAllData(String callingNumber, String IMSI, String fileName) {
        String[] roamingIdentification = {"07","05","05","05"};
        String[] IPAddresses = {"160.218.032.068", "193.016.219.096", "193.239.182.193", "193.027.231.001"};
        StringBuilder stringBuilder = new StringBuilder();

        for (int i = 0; i < IPAddresses.length; i++) {
            stringBuilder.append(cdrCreator.createDataCDR(callingNumber, IMSI, IPAddresses[i], 1024L, roamingIdentification[i])).append("\n");
        }
        fileManagement.createCDRFile(stringBuilder.toString(),fileName);
    }

    @Override
    public void generateSpecificData(String callingNumber, String IMSI, String IPAddress, String fileName, Long dataValue, String roamingIdentification) {
        fileManagement.createCDRFile(cdrCreator.createDataCDR(callingNumber, IMSI, IPAddress, dataValue, roamingIdentification), fileName);
    }
    @Override
    public void generateAllVoice(String callingNumber, String IMSI, String fileName) {
        String[] calledNumbers = {"420602123123", "420602123123", "420602123123", "420602123123", "420602302522", "420602123123", "420602123123", "420602123123", "420602123123", "420602123123", "420602123123"
                , "420602123123", "420223456789", "4201180", "4201181", "4201188", "4201224", "4201211", "42012123", "42013123", "42014112", "42014234", "420606000606", "420800000606", "420158"
                , "420116123", "420813456789", "420841456789", "420823456789", "420913456789", "420933456789", "420953456789", "420893456789", "447537410297", "460123123123", "355123123123", "100123123123"};

        String[] tariffClasses = {"2301", "2302", "2303", "2304", "2305", "2306", "2307", "2308", "2310", "2330", "2331", "2300"
                , "2300", "2300", "2300", "2300", "2300", "2300", "2300", "2300", "2300", "2300", "2300", "2300", "2300", "2300", "2300", "2300", "2300", "2300", "2300", "2300", "2300", "2300", "2302", "2302", "2302"};
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < calledNumbers.length; i++) {
            stringBuilder.append(cdrCreator.createVoiceCDR(callingNumber, IMSI, calledNumbers[i], tariffClasses[i])).append("\n");
        }
        fileManagement.createCDRFile(stringBuilder.toString(),fileName);
    }













}
