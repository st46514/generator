package com.o2.generatorCDR.service.CDR;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

@Service
public class CDRCreatorImpl implements CDRCreator {
    private SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
    private String timeStamp ;

    @Override
    public String createSMSCDR(String callingNumber, String IMSI, String calledNumber, String vmscNumber, String tariffClass) {
        timeStamp =  df.format(new Date().getTime());
        StringBuilder stringBuilder = new StringBuilder();
        String SMMO_RECORD_LENGTH = repeatString(5," ");
        String SMMO_RECORD_TYPE = "8" + repeatString(2," ");
        String SMMO_RECORD_NUMBER = repeatString(8," ");
        String SMMO_RECORD_STATUS = repeatString(3, " ");
        String SMMO_CHECK_SUM = repeatString(5, " ");
        String SMMO_CALL_REFERENCE = repeatString(10," ");
        String SMMO_EXCHANGE_ID_VMSC = repeatString(20," ");
        String SMMO_CALLING_IMSI = IMSI + repeatString(16-IMSI.length()," ");
        String SMMO_CALLING_IMEI = repeatString(16, " ");
        String SMMO_CALLING_NUMBER = callingNumber + repeatString(20-callingNumber.length()," ");
        String SMMO_CALLING_CATEGORY = repeatString(3," ");
        String SMMO_CALLING_MS_CLASSMARK = repeatString(3," ");
        String SMMO_DIALLED_DIGITS = repeatString(24," ");
        String SMMO_SMS_CENTRE = repeatString(20," ");
        String SMMO_CALLING_SUBS_LAC = repeatString(5," ");
        String SMMO_CALLING_SUBS_CI = repeatString(5," ");
        String SMMO_INCOMING_TIME = timeStamp;
        String SMMO_CAUSE_FOR_TERMINATION = repeatString(10," ");
        String SMMO_BASIC_SERVICE_TYPE = repeatString(3," ");
        String SMMO_BASIC_SERVICE_CODE = repeatString(3," ");
        String SMMO_CALL_TYPE = repeatString(3," ");
        String SMMO_MSC_TYPE = repeatString(3," ");
        String SMMO_CALLED_NUMBER  = calledNumber + repeatString(24 - calledNumber.length()," ");
        String SMMO_PNI = repeatString(6," ");
        String SMMO_CALLING_NUMBER_TON = "5" + repeatString(2," ");
        String SMMO_CALLED_NUMBER_TON = SMMO_CALLING_NUMBER_TON;
        String SMMO_CALLING_VMSC_NUMBER = vmscNumber + repeatString(20 - vmscNumber.length()," ");
        String SMMO_TARIFF_CLASS = tariffClass + repeatString(6 - tariffClass.length()," ");
        String SMMO_SMS_TYPE = repeatString(3," ");
        String SMMO_SMS_LENGTH = repeatString(3," ");
        String SMMO_COMMAND_TYPE = repeatString(3," ");
        String SMMO_MESSAGE_REFERENCE = repeatString(3," ");
        String SMMO_DELIVERY_TIME = repeatString(14," ");
        return stringBuilder.append(SMMO_RECORD_LENGTH).append(SMMO_RECORD_TYPE).append(SMMO_RECORD_NUMBER).append(SMMO_RECORD_STATUS).append(SMMO_CHECK_SUM).append(SMMO_CALL_REFERENCE)
                .append(SMMO_EXCHANGE_ID_VMSC).append(SMMO_CALLING_IMSI).append(SMMO_CALLING_IMEI).append(SMMO_CALLING_NUMBER).append(SMMO_CALLING_CATEGORY).append(SMMO_CALLING_MS_CLASSMARK)
                .append(SMMO_DIALLED_DIGITS).append(SMMO_SMS_CENTRE).append(SMMO_CALLING_SUBS_LAC).append(SMMO_CALLING_SUBS_CI).append(SMMO_INCOMING_TIME).append(SMMO_CAUSE_FOR_TERMINATION)
                .append(SMMO_BASIC_SERVICE_TYPE).append(SMMO_BASIC_SERVICE_CODE).append(SMMO_CALL_TYPE).append(SMMO_MSC_TYPE).append(SMMO_CALLED_NUMBER).append(SMMO_PNI).append(SMMO_CALLING_NUMBER_TON)
                .append(SMMO_CALLED_NUMBER_TON).append(SMMO_CALLING_VMSC_NUMBER).append(SMMO_TARIFF_CLASS).append(SMMO_SMS_TYPE).append(SMMO_SMS_LENGTH).append(SMMO_COMMAND_TYPE).append(SMMO_MESSAGE_REFERENCE)
                .append(SMMO_DELIVERY_TIME).toString();
    }

    @Override
    public String createDataCDR(String callingNumber, String IMSI, String IPAddress, Long dataValue, String roamingIdentification) {
        timeStamp =  df.format(new Date().getTime());
        StringBuilder stringBuilder = new StringBuilder();
        String download=String.valueOf(dataValue/2 + dataValue%2);
        String upload=String.valueOf(dataValue/2);

        String RECORD_TYPE = roamingIdentification;
        String A_IMSI = IMSI + repeatString(16-IMSI.length(), " ");
        String A_NUMBER = callingNumber + repeatString(18 - callingNumber.length(), " ");
        String B_NUMBER = repeatString(64, " ");
        String C_NUMBER = repeatString(64, " ");
        String START_TIME = timeStamp;
        String DURATION = repeatString(5,"0") + "1";
        String SERVICE_ID = repeatString(6, " ");
        String CALL_TYPE = repeatString(2, " ");
        String BEARER_TYPE = repeatString(2, " ");
        String VMSC_ADDR = IPAddress + repeatString(15 - IPAddress.length(), " ");
        String A_CGI = repeatString(14, " ");
        String TARIFF_CLASS = repeatString(4, " ");
        String MESSAGE_LENGTH = repeatString(4, " ");
        String VOLUME_DOWLOAD =  repeatString(10-download.length(), "0") + download ;
        String VOLUME_UPLOAD = repeatString(10-upload.length(), "0") + upload;
        String PRICE = repeatString(7, " ");
        String DISCOUNT_ABS = repeatString(7, " ");
        String DISCOUNT_PERCENTAGE = repeatString(3, " ");
        String DISCOUNT_ID = repeatString(1, " ");
        String EXCHANGE_ID = "160.218.032.068";
        String A_SUBS = repeatString(20, " ");
        String INTERMEDIATE_RECORD_NUMBER = repeatString(3, " ");
        String INTERMEDIATE_RECORD_INDICATOR = repeatString(1, " ");
        String RESERVED = " ";
        return stringBuilder.append(RECORD_TYPE).append(A_IMSI).append(A_NUMBER).append(B_NUMBER).append(C_NUMBER).append(START_TIME).append(DURATION).append(SERVICE_ID).append(CALL_TYPE)
                .append(BEARER_TYPE).append(VMSC_ADDR).append(A_CGI).append(TARIFF_CLASS).append(MESSAGE_LENGTH).append(VOLUME_DOWLOAD).append(VOLUME_UPLOAD).append(PRICE).append(DISCOUNT_ABS)
                .append(DISCOUNT_PERCENTAGE).append(DISCOUNT_ID).append(EXCHANGE_ID).append(A_SUBS).append(INTERMEDIATE_RECORD_NUMBER).append(INTERMEDIATE_RECORD_INDICATOR).append(RESERVED).toString();
    }

    @Override
    public String createVoiceCDR(String callingNumber, String IMSI, String calledNumber, String tariffClass) {
        timeStamp =  df.format(new Date().getTime());
        StringBuilder stringBuilder = new StringBuilder();
        String RECORD_TYPE = "3" + repeatString(2," ");
        String RECORD_NUMBER = repeatString(8," ");
        String RECORD_STATUS = repeatString(3," ");
        String EXCHANGE_ID = repeatString(20," ");
        String INTERMEDIATE_RECORD_NUMBER = "1" + repeatString(1," ");
        String INTERMEDIATE_CHARGING_IND = "0" + repeatString(2," ");
        String CALLING_IMSI = IMSI + repeatString( 16 - IMSI.length()," ");
        String CALLING_IMEI = repeatString(16," ");
        String CALLING_NUMBER = callingNumber + repeatString(20 - callingNumber.length()," ");
        String CALLING_NUMBER_TON = "5" + repeatString(2," ");
        String DIALLED_DIGITS = calledNumber + repeatString(24 - calledNumber.length()," ");
        String DIALLED_DIGITS_TON = "5" + repeatString(2," ");
        String CALLED_NUMBER = DIALLED_DIGITS;
        String CALLED_NUMBER_TON = DIALLED_DIGITS_TON;
        String BASIC_SERVICE_TYPE = repeatString(3," ");
        String BASIC_SERVICE_CODE = "17" + repeatString(1," ");
        String FACILITY_USAGE = repeatString(10," ");
        String CAUSE_FOR_TERMINATION = repeatString(10," ");
        String CHARGING_START_TIME = timeStamp;
        String ORIG_MCZ_DURATION = "60" + repeatString(4," ");
        String CALLING_SUBS_FIRST_LAC = repeatString(5," ");
        String CALLING_SUBS_FIRST_CI = repeatString(5," ");
        String ORIG_MCZ_TARIFF_CLASS = tariffClass + repeatString(6- tariffClass.length()," ");
        String GLOBAL_CALL_REFERENCE = timeStamp + "BABA" + callingNumber + repeatString(12," ");
        String CAUSE_FOR_FORWARDING = repeatString(3," ");
        String ORIG_CALLING_NUMBER = CALLING_NUMBER;
        String ORIG_CALLING_NUMBER_TON = CALLING_NUMBER_TON;

        return stringBuilder.append(RECORD_TYPE).append(RECORD_NUMBER).append(RECORD_STATUS).append(EXCHANGE_ID).append(INTERMEDIATE_RECORD_NUMBER).append(INTERMEDIATE_CHARGING_IND)
                .append(CALLING_IMSI).append(CALLING_IMEI).append(CALLING_NUMBER).append(CALLING_NUMBER_TON).append(DIALLED_DIGITS).append(DIALLED_DIGITS_TON).append(CALLED_NUMBER)
                .append(CALLED_NUMBER_TON).append(BASIC_SERVICE_TYPE).append(BASIC_SERVICE_CODE).append(FACILITY_USAGE).append(CAUSE_FOR_TERMINATION).append(CHARGING_START_TIME)
                .append(ORIG_MCZ_DURATION).append(CALLING_SUBS_FIRST_LAC).append(CALLING_SUBS_FIRST_CI).append(ORIG_MCZ_TARIFF_CLASS).append(GLOBAL_CALL_REFERENCE).append(CAUSE_FOR_FORWARDING)
                .append(ORIG_CALLING_NUMBER).append(ORIG_CALLING_NUMBER_TON).toString();

    }


    @Override
    public Document createXML(String callingNumber, String IMSI, String senderKey, String receiverKey,String[] calledNumbers) {
        timeStamp =  df.format(new Date().getTime());
        Random random = new Random();
        int sequenceNumber = 1 + random.nextInt(99999);
        Document document = DocumentHelper.createDocument();

        Element DataInterChange = document.addElement("DataInterChange");
        Element transferBatch = DataInterChange.addElement("transferBatch");
        Element batchControlInfo = transferBatch.addElement("batchControlInfo");

        Element sender = batchControlInfo.addElement("sender").addText(senderKey);
        Element recipient = batchControlInfo.addElement("recipient").addText(receiverKey);
        Element fileSequenceNumber = batchControlInfo.addElement("fileSequenceNumber").addText(String.valueOf(sequenceNumber));

        Element fileCreationTimeStamp = batchControlInfo.addElement("fileCreationTimeStamp");
        Element localTimeStamp = fileCreationTimeStamp.addElement("localTimeStamp").addText(timeStamp);
        Element utcTimeOffset = fileCreationTimeStamp.addElement("utcTimeOffset").addText("+0000");

        Element transferCutOffTimeStamp = batchControlInfo.addElement("transferCutOffTimeStamp");
        Element localTimeStamp1 = transferCutOffTimeStamp.addElement("localTimeStamp").addText(timeStamp);
        Element utcTimeOffset1 = transferCutOffTimeStamp.addElement("utcTimeOffset").addText("+0000");

        Element fileAvailableTimeStamp = batchControlInfo.addElement("fileAvailableTimeStamp");
        Element localTimeStamp2 = fileAvailableTimeStamp.addElement("localTimeStamp").addText(timeStamp);
        Element utcTimeOffset2 = fileAvailableTimeStamp.addElement("utcTimeOffset").addText("+0000");

        Element callEventDetails = transferBatch.addElement("callEventDetails");

        for (String number: calledNumbers) {
            Element mobileOriginatedCall = mobileOriginatedCallCreator(callEventDetails, IMSI, callingNumber, number);
        }
        Element mobileTerminatedCall = callEventDetails.addElement("mobileTerminatedCall");
        Element basicCallInformation = mobileTerminatedCall.addElement("basicCallInformation");

        Element simChargeableSubscriber = basicCallInformation.addElement("simChargeableSubscriber");
        Element imsi = simChargeableSubscriber.addElement("imsi").addText(IMSI);
        Element msisdn = simChargeableSubscriber.addElement("msisdn").addText(callingNumber);

        Element callOriginator = basicCallInformation.addElement("callOriginator").addElement("callingNumber").addText("48949726111");

        Element callEventStartTimeStamp = basicCallInformation.addElement("callEventStartTimeStamp");
        Element localTimeStamp3 = callEventStartTimeStamp.addElement("localTimeStamp").addText(timeStamp);
        Element utcTimeOffset3 = callEventStartTimeStamp.addElement("utcTimeOffset").addText("+0000");

        Element totalCallEventDuration = basicCallInformation.addElement("totalCallEventDuration").addText("60");

        Element locationInformation = mobileTerminatedCall.addElement("locationInformation");
        Element networkLocation = locationInformation.addElement("networkLocation");
        Element recEntityCode = networkLocation.addElement("recEntityCode").addText("7");
        Element callReference = networkLocation.addElement("callReference").addText("4369180c05");
        Element locationArea = networkLocation.addElement("locationArea").addText("86");
        Element cellId = networkLocation.addElement("cellId").addText("32701");

        Element equipmentInformation = mobileTerminatedCall.addElement("equipmentInformation").addElement("imei").addText("358087047501790");

        Element basicServiceUsedList = mobileTerminatedCall.addElement("basicServiceUsedList");
        Element BasicServiceUsed = basicServiceUsedList.addElement("BasicServiceUsed");

        Element basicService = BasicServiceUsed.addElement("basicService").addElement("teleServiceCode").addText("11");

        Element chargeInformationList = BasicServiceUsed.addElement("chargeInformationList");
        Element ChargeInformation = chargeInformationList.addElement("ChargeInformation");
        Element chargedItem = ChargeInformation.addElement("chargedItem").addText("D");
        Element exchangeRateCode = ChargeInformation.addElement("exchangeRateCode").addText("0");

        Element chargeDetailList = ChargeInformation.addElement("chargeDetailList");
        Element ChargeDetail = chargeDetailList.addElement("ChargeDetail");
        Element chargeType = ChargeDetail.addElement("chargeType").addText("00");
        Element charge = ChargeDetail.addElement("chargeType").addText("0");
        Element chargeableUnits = ChargeDetail.addElement("chargeableUnits").addText("66");
        Element chargedUnits = ChargeDetail.addElement("chargedUnits").addText("66");
        Element dayCategory = ChargeDetail.addElement("dayCategory").addText("I");
        Element timeBand = ChargeDetail.addElement("timeBand").addText("I");

        return document;

    }

    private String repeatString(int number, String string){
        if (number == 0)
            return "";
        return String.format("%0" + number + "d", 0).replace("0",string);
    }

    private Element mobileOriginatedCallCreator(Element callEventDetails, String imsiNumber, String msisdnNumber, String calledMSISDN){
        Element mobileOriginatedCall = callEventDetails.addElement("mobileOriginatedCall");
        Element basicCallInformation = mobileOriginatedCall.addElement("basicCallInformation");

        Element simChargeableSubscriber = basicCallInformation.addElement("simChargeableSubscriber");
        Element imsi = simChargeableSubscriber.addElement("imsi").addText(imsiNumber);
        Element msisdn = simChargeableSubscriber.addElement("msisdn").addText(msisdnNumber);

        Element destination = basicCallInformation.addElement("destination");
        Element calledNumber = destination.addElement("calledNumber").addText(calledMSISDN);
        Element dialledDigits = destination.addElement("dialledDigits").addText(calledMSISDN);

        Element callEventStartTimeStamp = basicCallInformation.addElement("callEventStartTimeStamp");
        Element localTimeStamp = callEventStartTimeStamp.addElement("localTimeStamp").addText(timeStamp);
        Element utcTimeOffsetCode = callEventStartTimeStamp.addElement("utcTimeOffsetCode").addText("+0000");

        Element totalCallEventDuration = basicCallInformation.addElement("totalCallEventDuration").addText("60");

        Element locationInformation = mobileOriginatedCall.addElement("locationInformation");
        Element networkLocation = locationInformation.addElement("networkLocation");
        Element recEntityCode = networkLocation.addElement("recEntityCode").addText("0");
        Element callReference = networkLocation.addElement("callReference").addText("1b4d08");
        Element locationArea = networkLocation.addElement("locationArea").addText("213");
        Element cellId = networkLocation.addElement("cellId").addText("12052");

        Element equipmentInformation = mobileOriginatedCall.addElement("equipmentInformation");
        Element mobileStationClassMark = equipmentInformation.addElement("mobileStationClassMark").addText("1");
        Element imei = equipmentInformation.addElement("imei").addText("357194041943710");

        Element basicServiceUsedList = mobileOriginatedCall.addElement("basicServiceUsedList");
        Element BasicServiceUsed = basicServiceUsedList.addElement("BasicServiceUsed");
        Element basicService = BasicServiceUsed.addElement("basicService");
        Element teleServiceCode = basicService.addElement("teleServiceCode").addText("11");
        Element radioChannelRequested = basicService.addElement("radioChannelRequested").addText("0");
        Element radioChannelUsed = basicService.addElement("radioChannelUsed").addText("0");
        Element transparencyIndicator = basicService.addElement("transparencyIndicator").addText("0");

        Element chargeInformationList = BasicServiceUsed.addElement("chargeInformationList");
        Element ChargeInformation = chargeInformationList.addElement("ChargeInformation");
        Element chargedItem = ChargeInformation.addElement("chargedItem").addText("D");
        Element exchangeRateCode = ChargeInformation.addElement("exchangeRateCode").addText("0");
        Element callTypeGroup = ChargeInformation.addElement("callTypeGroup");
        Element callTypeLevel1 = callTypeGroup.addElement("callTypeLevel1").addText("2");
        Element callTypeLevel2 = callTypeGroup.addElement("callTypeLevel2").addText("0");
        Element callTypeLevel3 = callTypeGroup.addElement("callTypeLevel3").addText("0");
        Element calledCountryCode = callTypeGroup.addElement("calledCountryCode").addText("CZE");

        Element chargeDetailList = ChargeInformation.addElement("chargeDetailList");
        Element ChargeDetail = chargeDetailList.addElement("ChargeDetail");
        Element chargeType = ChargeDetail.addElement("chargeType").addText("00");
        Element charge = ChargeDetail.addElement("charge").addText("952");
        Element chargeableUnits = ChargeDetail.addElement("chargeableUnits").addText("120");
        Element chargedUnits = ChargeDetail.addElement("chargedUnits").addText("120");
        Element dayCategory = ChargeDetail.addElement("dayCategory").addText("I");
        Element timeBand = ChargeDetail.addElement("timeBand").addText("I");

        Element taxInformation = ChargeInformation.addElement("taxInformation");
        Element TaxInformation = taxInformation.addElement("TaxInformation");
        Element taxCode = TaxInformation.addElement("taxCode").addText("0");
        Element taxValue = TaxInformation.addElement("taxValue").addText("0");

        return mobileOriginatedCall;
    }
}
