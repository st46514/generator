package com.o2.generatorCDR.service.Invoice;

import com.o2.generatorCDR.entity.InvoiceData;
import com.o2.generatorCDR.entity.InvoiceHeader;
import com.o2.generatorCDR.entity.InvoiceTax;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class InvoiceCreator  {

    private List<InvoiceData> invoiceDataList = new ArrayList<>();
    private List<InvoiceHeader> invoiceHeaderList = new ArrayList<>();
    private List<InvoiceTax> invoiceTaxList = new ArrayList<>();



    private InvoiceData createInvoiceData(String[] lineOfCSV) {

        return new InvoiceData(lineOfCSV[0],lineOfCSV[1],lineOfCSV[2],lineOfCSV[3],lineOfCSV[4],lineOfCSV[5],lineOfCSV[6],lineOfCSV[7]);
    }


    private InvoiceHeader createInvoiceHeader(String[] lineOfCSV) {

        return new InvoiceHeader(lineOfCSV[0],lineOfCSV[1],lineOfCSV[2],lineOfCSV[3],lineOfCSV[4],lineOfCSV[5],lineOfCSV[6],lineOfCSV[7],lineOfCSV[8],lineOfCSV[9]);
    }


    private InvoiceTax createInvoiceTax(String[] lineOfCSV) {

        return new InvoiceTax(lineOfCSV[0],lineOfCSV[1],lineOfCSV[2],lineOfCSV[3],lineOfCSV[4],lineOfCSV[5],lineOfCSV[6]);
    }

    public List<InvoiceData> getInvoiceDataList() {
        return invoiceDataList;
    }

    public List<InvoiceHeader> getInvoiceHeaderList() {
        return invoiceHeaderList;
    }

    public List<InvoiceTax> getInvoiceTaxList() {
        return invoiceTaxList;
    }

    public void switchInvoice(String[] line) {

        if (line[2].equals(""))
            line[2]=" ";
        switch (line[2].charAt(0)) {
            case 'H':
                InvoiceHeader invoiceHeader = createInvoiceHeader(line);
                invoiceHeaderList.add(invoiceHeader);
                break;
            case 'D':
                InvoiceData invoiceData = createInvoiceData(line);
                invoiceDataList.add(invoiceData);
                break;
            case 'T':
                InvoiceTax invoiceTax = createInvoiceTax(line);
                invoiceTaxList.add(invoiceTax);
                break;
            default:

                break;
        }
    }
}
