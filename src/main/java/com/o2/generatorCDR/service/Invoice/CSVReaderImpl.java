package com.o2.generatorCDR.service.Invoice;

import com.o2.generatorCDR.entity.SummaryCredit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
@Service
public class CSVReaderImpl implements CSVReader {
    @Autowired
    private InvoiceCreator invoiceCreator;
    @Autowired
    private DetailCreator detailCreator;
    @Autowired
    private SummaryCreator summaryCreator;
    public void readCSV(File file) {
        BufferedReader bufferedReader = null;
        if (file != null) {
            try {

                FileReader fileReader = new FileReader(file);
                bufferedReader = new BufferedReader(fileReader);
                String stringLine;

                while ((stringLine = bufferedReader.readLine()) != null) {

                    //System.out.println(stringLine);

                    String[] line = stringLine.split("\\|",-1);
                    if (line[0].equals(""))
                        line[0] = " ";
                    switch (line[0].charAt(0)) {
                        case 'I':
                            invoiceCreator.switchInvoice(line);
                            break;
                        case 'S':
                            summaryCreator.switchSummary(line);
                            break;
                        case 'D':
                            detailCreator.switchDetail(line);
                            break;
                        default:
                            // System.out.println("baba");
                            break;
                    }
                }
            } catch (IOException exception) {

                exception.fillInStackTrace();

            } finally {
                try {
                    if (bufferedReader != null)
                        bufferedReader.close();
                    System.out.println("Konec");

                } catch (IOException exception) {

                    exception.fillInStackTrace();

                }
            }
        }
    }
}
