package com.o2.generatorCDR.service.Invoice;

import com.o2.generatorCDR.entity.*;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SummaryCreator {

    private List<SummaryHeader> summaryHeaderList = new ArrayList<>();
    private List<SummaryFreeUnits> summaryFreeUnitsList = new ArrayList<>();
    private List<SummaryRecurrentFee> summaryRecurrentFeeList = new ArrayList<>();
    private List<SummaryUsage> summaryUsageList = new ArrayList<>();
    private List<SummaryPartyServices> summaryPartyServicesList = new ArrayList<>();
    private List<SummaryCredit> summaryCreditList = new ArrayList<>();

    public List<SummaryHeader> getSummaryHeaderList() {
        return summaryHeaderList;
    }

    public List<SummaryFreeUnits> getSummaryFreeUnitsList() {
        return summaryFreeUnitsList;
    }

    public List<SummaryRecurrentFee> getSummaryRecurrentFeeList() {
        return summaryRecurrentFeeList;
    }

    public List<SummaryUsage> getSummaryUsageList() {
        return summaryUsageList;
    }

    public List<SummaryPartyServices> getSummaryPartyServicesList() {
        return summaryPartyServicesList;
    }

    public List<SummaryCredit> getSummaryCreditList() {
        return summaryCreditList;
    }

    private SummaryHeader createSummaryHeader(String[] lineOfCSV){
        return new SummaryHeader(lineOfCSV[0],lineOfCSV[1],lineOfCSV[2],lineOfCSV[3],lineOfCSV[4],lineOfCSV[5],lineOfCSV[6],lineOfCSV[7],lineOfCSV[8],lineOfCSV[9],lineOfCSV[10]);
    }

    private SummaryFreeUnits createSummaryFreeUnits(String[] lineOfCSV){
        return new SummaryFreeUnits(lineOfCSV[0],lineOfCSV[1],lineOfCSV[2],lineOfCSV[3],lineOfCSV[4],lineOfCSV[5],lineOfCSV[6],lineOfCSV[7],lineOfCSV[8]);
    }

    private SummaryRecurrentFee createSummaryRecurrentFee(String[] lineOfCSV){
        return new SummaryRecurrentFee(lineOfCSV[0],lineOfCSV[1],lineOfCSV[2],lineOfCSV[3],lineOfCSV[4],lineOfCSV[5],lineOfCSV[6],lineOfCSV[7],lineOfCSV[8],lineOfCSV[9],lineOfCSV[10]
                ,lineOfCSV[11],lineOfCSV[12],lineOfCSV[13],lineOfCSV[14],lineOfCSV[15],lineOfCSV[16]);
    }

    private SummaryUsage createSummaryUsage(String[] lineOfCSV){
        return new SummaryUsage(lineOfCSV[0],lineOfCSV[1],lineOfCSV[2],lineOfCSV[3],lineOfCSV[4],lineOfCSV[5],lineOfCSV[6],lineOfCSV[7],lineOfCSV[8],lineOfCSV[9],lineOfCSV[10]
                ,lineOfCSV[11],lineOfCSV[12],lineOfCSV[13],lineOfCSV[14],lineOfCSV[15],lineOfCSV[16],lineOfCSV[17],lineOfCSV[18],lineOfCSV[19],lineOfCSV[20],lineOfCSV[21],lineOfCSV[22]);
    }

    private SummaryPartyServices createSummaryPartyServices(String[] lineOfCSV){
        return new SummaryPartyServices(lineOfCSV[0],lineOfCSV[1],lineOfCSV[2],lineOfCSV[3],lineOfCSV[4],lineOfCSV[5],lineOfCSV[6],lineOfCSV[7],lineOfCSV[8],lineOfCSV[9],lineOfCSV[10]
                ,lineOfCSV[11],lineOfCSV[12],lineOfCSV[13],lineOfCSV[14],lineOfCSV[15],lineOfCSV[16],lineOfCSV[17],lineOfCSV[18]);
    }

    private SummaryCredit createSummaryCredit(String[] lineOfCSV){
        return new SummaryCredit(lineOfCSV[0],lineOfCSV[1],lineOfCSV[2],lineOfCSV[3],lineOfCSV[4],lineOfCSV[5],lineOfCSV[6],lineOfCSV[7],lineOfCSV[8],lineOfCSV[9],lineOfCSV[10],lineOfCSV[11]);
    }



    public void switchSummary(String[] line) {

        if (line[2].equals(""))
            line[2] = " ";
        switch (line[2].charAt(0)) {
            case 'H':
                SummaryHeader summaryHeader = createSummaryHeader(line);
                summaryHeaderList.add(summaryHeader);
                break;
            case 'F':
                SummaryFreeUnits summaryFreeUnits = createSummaryFreeUnits(line);
                summaryFreeUnitsList.add(summaryFreeUnits);
                break;
            case 'R':
                SummaryRecurrentFee summaryRecurrentFee = createSummaryRecurrentFee(line);
                summaryRecurrentFeeList.add(summaryRecurrentFee);
                break;
            case 'U':
                SummaryUsage summaryUsage = createSummaryUsage(line);
                summaryUsageList.add(summaryUsage);
                break;
            case 'S':
                SummaryPartyServices summaryPartyServices = createSummaryPartyServices(line);
                summaryPartyServicesList.add(summaryPartyServices);
                break;
            case 'O':
                SummaryCredit summaryCredit = createSummaryCredit(line);
                summaryCreditList.add(summaryCredit);
                break;
            default:

                break;
        }
    }

}
