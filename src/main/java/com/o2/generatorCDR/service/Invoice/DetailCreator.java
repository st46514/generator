package com.o2.generatorCDR.service.Invoice;

import com.o2.generatorCDR.entity.DetailData;
import com.o2.generatorCDR.entity.DetailHeader;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
@Service
public class DetailCreator {
    private List<DetailHeader> detailHeaderList = new ArrayList<>();
    private List<DetailData> detailDataList = new ArrayList<>();

    public List<DetailHeader> getDetailHeaderList() {
        return detailHeaderList;
    }

    public List<DetailData> getDetailDataList() {
        return detailDataList;
    }

    private DetailHeader createDetailHeader(String[] lineOfCSV){
        return new DetailHeader(lineOfCSV[0],lineOfCSV[1],lineOfCSV[2],lineOfCSV[3],lineOfCSV[4],lineOfCSV[5]);
    }

    private DetailData createDetailData(String[] lineOfCSV){
        return new DetailData(lineOfCSV[0],lineOfCSV[1],lineOfCSV[2],lineOfCSV[3],lineOfCSV[4],lineOfCSV[5],lineOfCSV[6],lineOfCSV[7],lineOfCSV[8],lineOfCSV[9],lineOfCSV[10]
                , lineOfCSV[11], lineOfCSV[12], lineOfCSV[13], lineOfCSV[14], lineOfCSV[15], lineOfCSV[16], lineOfCSV[17], lineOfCSV[18], lineOfCSV[19], lineOfCSV[20], lineOfCSV[21], lineOfCSV[22], lineOfCSV[23], lineOfCSV[24]);
    }

    public void switchDetail(String[] line) {

        if (line[2].equals(""))
            line[2] = " ";
        switch (line[2].charAt(0)) {
            case 'H':
                DetailHeader detailHeader = createDetailHeader(line);
                detailHeaderList.add(detailHeader);
                break;
            case 'D':
                DetailData detailData = createDetailData(line);
                detailDataList.add(detailData);
                break;
            default:

                break;
        }
    }


}
