package com.o2.generatorCDR.service.Invoice;

import com.o2.generatorCDR.entity.InvoiceData;
import com.o2.generatorCDR.entity.InvoiceHeader;
import com.o2.generatorCDR.entity.InvoiceTax;

import javax.swing.text.html.parser.Entity;
import java.io.File;

public interface CSVReader {

    void readCSV(File file);

}
